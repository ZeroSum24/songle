package com.example.fdsfsdf.songle.manager;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Local unit test for the PointsSystem class, which will execute on the development machine (host).
 */
public class PointsSystemTest {


    /**
     * This method asserts that the returned points for the the marker interest level are correct
     *
     * @throws Exception if the test leads to one
     */
    @Test
    public void returnEarnedPointsTest() throws Exception {

        PointsSystem testPS = new PointsSystem();

        String markerInterestLevel = "#veryinteresting";
        String expectedPoints = Integer.toString(100);
        String returnedPoints = testPS.returnEarnedPoints(markerInterestLevel);

        assertEquals(returnedPoints, expectedPoints);

    }

    /**
     * This method asserts that the error is correct if the returned points for the the marker
     * interest level are incorrect
     *
     * @throws Exception if the test leads to one
     */
    @Test(expected = Error.class)
    public void returnEarnedPointsErrorTest() throws Exception {

        PointsSystem testPS = new PointsSystem();

        String markerInterestLevel = "#unknown";

        try {
            testPS.returnEarnedPoints(markerInterestLevel);
        } catch (Exception e) {
            String message = "StyleUrl feed to Earned Points does not exist";
            assertEquals(message, e.getMessage());
            throw e;
        }
    }


    /**
     * This method asserts that the new total points are correct after a certain amount of guesses
     *
     * @throws Exception if the test leads to one
     */
    @Test
    public void addToGuessNewTPointsTest() throws Exception {

        PointsSystem testPS = new PointsSystem();

        int amountOfGuesses = 3;
        int totalSongPoints = testPS.getTotalSongPoints();

        int expectedPoints = 250;
        testPS.addToGuessNewTPoints(amountOfGuesses);
        int updatedTotalSongPoints = testPS.getTotalSongPoints();

        boolean condition = updatedTotalSongPoints == (totalSongPoints+expectedPoints);
        assertTrue("Expected Points have been added to the Total Songs Points", condition);

    }
    /**
     * This method asserts that the new total points are correct after more than 5 guesses
     *
     * @throws Exception if the test leads to one
     */
    @Test
    public void addToGuessNewTPointsMoreThan5Test() throws Exception {

        PointsSystem testPS = new PointsSystem();

        int amountOfGuesses = 6;
        int totalSongPoints = testPS.getTotalSongPoints();

        int expectedPoints = 50;
        testPS.addToGuessNewTPoints(amountOfGuesses);
        int updatedTotalSongPoints = testPS.getTotalSongPoints();

        boolean condition = updatedTotalSongPoints == (totalSongPoints+expectedPoints);
        assertTrue("Expected Points have been added to the Total Songs Points", condition);

    }

    /**
     * This method asserts that the correct difficulty bonus is calculated if the difficulty has
     * been increased
     *
     * @throws Exception if the test leads to one
     */
    @Test
    public void difficultyBonusIncreaseTest() throws Exception {

        PointsSystem testPS = new PointsSystem();

        int initialDifficulty = 2;
        int currentDifficulty = 4;
        int changeInDifficulty = 2;

        int totalSongPoints = testPS.getTotalSongPoints();

        double expectedMultiplier = 1.25;
        testPS.difficultyBonusNewTPoints(currentDifficulty,changeInDifficulty,initialDifficulty);
        int updatedTotalSongPoints = testPS.getTotalSongPoints();

        boolean condition = updatedTotalSongPoints == (totalSongPoints*expectedMultiplier);
        assertTrue("Multiplier has been applied correctly", condition);

    }
    /**
     * This method asserts that the correct difficulty bonus is calculated if the difficulty has
     * been decreased
     *
     * @throws Exception if the test leads to one
     */
    @Test
    public void difficultyBonusDecreaseTest() throws Exception {

        PointsSystem testPS = new PointsSystem();

        int initialDifficulty = 2;
        int currentDifficulty = 1;
        int changeInDifficulty = 1;

        int totalSongPoints = testPS.getTotalSongPoints();

        double expectedMultiplier = 1;
        testPS.difficultyBonusNewTPoints(currentDifficulty,changeInDifficulty,initialDifficulty);
        int updatedTotalSongPoints = testPS.getTotalSongPoints();

        boolean condition = updatedTotalSongPoints == (totalSongPoints*expectedMultiplier);
        assertTrue("Multiplier has been applied correctly", condition);

    }

    /**
     * This method asserts that the correct difficulty bonus is calculated if the difficulty has
     * not been changed
     *
     * @throws Exception if the test leads to one
     */
    @Test
    public void difficultyBonusNoChangeTest() throws Exception {

        PointsSystem testPS = new PointsSystem();

        int initialDifficulty = 4;
        int currentDifficulty = 4;
        int changeInDifficulty = 0;

        int totalSongPoints = testPS.getTotalSongPoints();

        double expectedMultiplier = 2.0;
        testPS.difficultyBonusNewTPoints(currentDifficulty,changeInDifficulty,initialDifficulty);
        int updatedTotalSongPoints = testPS.getTotalSongPoints();

        boolean condition = updatedTotalSongPoints == (totalSongPoints*expectedMultiplier);
        assertTrue("Multiplier has been applied correctly", condition);

    }

    /**
     * This method asserts that the correct difficulty bonus is calculated if the change in
     * difficulty has not been entered correctly
     *
     * @throws Exception if the test leads to one
     */
    @Test
    public void difficultyBonusIsWrongTest() throws Exception {

        PointsSystem testPS = new PointsSystem();

        int initialDifficulty = 2;
        int currentDifficulty = 4;
        int changeInDifficulty = 0;

        int totalSongPoints = testPS.getTotalSongPoints();

        double expectedMultiplier = 2.0;
        testPS.difficultyBonusNewTPoints(currentDifficulty,changeInDifficulty,initialDifficulty);
        int updatedTotalSongPoints = testPS.getTotalSongPoints();

        boolean condition = updatedTotalSongPoints == (totalSongPoints*expectedMultiplier);

        assertTrue("Multiplier has been applied incorrectly", condition);
    }

    /**
     * This method asserts that the points to be submitted are calculated correctly if the overlay
     * has set the total song points
     *
     * @throws Exception if the test leads to one
     */
    @Test
    public void pointsToSubmitOverlayTest() throws Exception {

        PointsSystem testPS = new PointsSystem();
        testPS.setTotalPoints(3020);
        testPS.setTotalSongPoints(2000);
        testPS.setCurrentPoints(20);

        int expected = 2000+3020;

        assertEquals(testPS.pointsToSubmit(), expected);

    }

    /**
     * This method asserts that the points to be submitted are calculated correctly if the overlay
     * has not set the total song points. Instead this represents a quit from the game.
     *
     * @throws Exception if the test leads to one
     */
    @Test
    public void pointsToSubmitQuitTest() throws Exception {

        PointsSystem testPS = new PointsSystem();
        testPS.setTotalPoints(3020);
        testPS.setCurrentPoints(20);

        int expected = 20+3020;

        assertEquals(testPS.pointsToSubmit(), expected);

    }

}


