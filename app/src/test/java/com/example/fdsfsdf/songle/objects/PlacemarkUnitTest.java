package com.example.fdsfsdf.songle.objects;

import com.example.fdsfsdf.songle.R;

import org.junit.Test;

import static junit.framework.Assert.fail;
import static org.junit.Assert.assertEquals;

/**
 * Local unit test for the Placemark class, which will execute on the development machine (host).
 */
public class PlacemarkUnitTest {


    private Placemark test1 = new Placemark("34:1", "boring", "#boring", new Point("-3.1896948527782767,55.945066395849906,0"));
    private Placemark test2 = new Placemark("9:4", "notboring", "#notboring", new Point("-3.191022482076821,55.945409920152805,0"));
    private Placemark test3 = new Placemark("30:7", "interesting", "#interesting", new Point("-3.1854195299326578,55.94619505458477,0"));
    private Placemark test4 = new Placemark("38:1", "veryinteresting", "#veryinteresting", new Point("-3.187150971691298,55.944928787507585,0"));
    private Placemark test5 = new Placemark("21:7", "unclassified", "#unclassified", new Point("-3.186669178092598,55.94444913310533,0"));

    private Placemark testIncorrect = new Placemark("21:7", "unknown", "unknown", new Point("-3.186669178092598,55.94444913310533,0"));

    @Test
    public void haveCorrectDescription() throws Exception {

        assertEquals(test1.getDescription(), R.string.iBoring);
        assertEquals(test2.getDescription(), R.string.iNotBoring);
        assertEquals(test3.getDescription(), R.string.iInteresting);
        assertEquals(test4.getDescription(), R.string.iVeryInteresting);
        assertEquals(test5.getDescription(), R.string.iUnclassified);

    }

    @Test(expected = Error.class)
    public void haveIncorrectDescription() throws Exception {

        try {
            testIncorrect.getDescription();
        } catch (Error e) {
            String message = "Description does not exist";
            assertEquals(message, e.getMessage());
            throw e;
        }
    }

    @Test
    public void haveCorrectMarkerImage() throws Exception {

        assertEquals(test1.returnMarkerImage(), R.drawable.map_icon_boring);
        assertEquals(test2.returnMarkerImage(), R.drawable.map_icon_notboring);
        assertEquals(test3.returnMarkerImage(), R.drawable.map_icon_interesting);
        assertEquals(test4.returnMarkerImage(), R.drawable.map_icon_veryinteresting);
        assertEquals(test5.returnMarkerImage(), R.drawable.map_icon_unclassified);
    }

    @Test(expected = Error.class)
    public void haveIncorrectMarkerImage() throws Exception {
        try {
            testIncorrect.returnMarkerImage();
        } catch (Error re) {
            String message = "StyleUrl does not exist";
            assertEquals(message, re.getMessage());
            throw re;
        }
    }


}

