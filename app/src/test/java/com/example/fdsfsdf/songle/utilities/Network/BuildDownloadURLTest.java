package com.example.fdsfsdf.songle.utilities.Network;

import org.junit.Test;
import java.net.URL;
import static junit.framework.Assert.assertEquals;


/**
 * Local unit test for the BuildDownloadURL class, which will execute on the development machine (host).
 */
public class BuildDownloadURLTest {

    /**
     * This method asserts that the map url for an example map is as expected
     *
     * @throws Exception
     */
    @Test
    public void buildMapUrl() throws Exception {

        URL expected = new URL
                ( "http://www.inf.ed.ac.uk/teaching/courses/selp/data/songs/02/map3.kml");

        String file = "map_.kml";
        String songNumber = "02";
        String mapNumber = "3";

        URL output = BuildDownloadURL.buildUrl(file,songNumber,mapNumber);

        assertEquals(expected,output);
    }

    /**
     * This method asserts that the lyrics url for an example song is as expected
     *
     * @throws Exception
     */
    @Test
    public void buildLyricsUrl() throws Exception {

        URL expected = new URL
                ( "http://www.inf.ed.ac.uk/teaching/courses/selp/data/songs/02/lyrics.txt");

        String file = "lyrics.txt";
        String songNumber = "02";
        String mapNumber = null;

        URL output = BuildDownloadURL.buildUrl(file,songNumber,mapNumber);

        assertEquals(expected,output);
    }

    /**
     * This method asserts that the url for songs.xml is as expected
     *
     * @throws Exception
     */
    @Test
    public void buildSongsUrl() throws Exception {

        URL expected = new URL
                ( "http://www.inf.ed.ac.uk/teaching/courses/selp/data/songs/songs.xml");

        String file = "songs.xml";
        String songNumber = null;
        String mapNumber = null;

        URL output = BuildDownloadURL.buildUrl(file,songNumber,mapNumber);

        assertEquals(expected,output);
    }
}
