package com.example.fdsfsdf.songle.objects;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Local unit test for the Point class, which will execute on the development machine (host).
 */
public class PointUnitTest {

    private Point test1 = new Point("-3.1896948527782767,55.945066395849906,0");
    private Point test2 = new Point("-3.191022482076821,55.945409920152805,0");
    private Point test3 = new Point("-3.1854195299326578,55.94619505458477,0");

    @Test
    public void returnCorrectCoordinates() throws Exception {

        Double test1Long = -3.1896948527782767;
        Double test1Lat = 55.945066395849906;

        Double test2Long = -3.191022482076821;
        Double test2Lat = 55.945409920152805;

        Double test3Long = -3.1854195299326578;
        Double test3Lat = 55.94619505458477;


        assertTrue(test1.getLong() == test1Long);
        assertTrue(test1.getLat() == test1Lat);

        assertTrue(test2.getLong() == test2Long);
        assertTrue(test2.getLat() == test2Lat);

        assertTrue(test3.getLong() == test3Long);
        assertTrue(test3.getLat() == test3Lat);

}

}