package com.example.fdsfsdf.songle.manager;


import com.example.fdsfsdf.songle.objects.Song;
import com.example.fdsfsdf.songle.objects.Words;

import org.junit.Before;
import org.junit.Test;
import java.util.Arrays;
import java.util.List;


import static org.junit.Assert.assertTrue;

/**
 * Local unit test for the GameCreator class, which will execute on the development machine (host).
 */
public class GameCreatorUnitTest {

    private Song testData1 = new Song("01", null,null,null);
    private Song testData2 = new Song("02", null,null,null);
    private Song testData3 = new Song("03", null,null,null);
    private Song testData4 = new Song("04", null,null,null);
    private Song testData5 = new Song("05", null,null,null);
    private Song testData6 = new Song("06", null,null,null);
    private Song testData7 = new Song("07", null,null,null);
    private Song testData8 = new Song("08", null,null,null);

    private List<Song> songList = Arrays.asList(testData1,testData2,testData3,
            testData4,testData5,testData6,testData7,testData8);

    private List<List<String>> wordsList =
            Arrays.asList(
                    Arrays.asList("Tom", "John", "Mary", "Peter"),
                    Arrays.asList("Tom", "John", "Mary", "Peter"),
                    Arrays.asList("Tom", "John", "Mary", "Peter", "Mary", "Peter"),
                    Arrays.asList(""),
                    Arrays.asList("Tom", "John", "Mary", "Peter", "Mary", "Peter", "Mary", "Peter"),
                    Arrays.asList("Tom", "John", "Mary", "Peter", "Mary", "Peter"),
                    Arrays.asList("Tom", "John", "Mary"),
                    Arrays.asList("Tom", "John", "Mary", "Peter", "Mary")
            );

    private Words testWords = new Words(wordsList);

    /**
     * Method sets-up variables needed for the chooseAndSkipPreviousSongWorks() which involves
     * resetting the skipped song so that it can be played later.
     */
    @Before
    public void setUp() {
        for (Song current : songList) {
            current.setWords(testWords);
            current.getWords().getWord("3:1", true);
            current.getWords().getWord("6:4", true);
        }
    }

    /**
     * This method asserts that the chooseSong() method is choosing a song which is contained
     * within the song list
     *
     * @throws Exception if the test leads to one
     */
    @Test
    public void chooseSongWorks() throws Exception {

    GameCreator tester = new GameCreator();

    tester.setSongList(songList);

    tester.chooseSong();
    boolean condition = songList.contains(tester.getCurrentSong());

    assertTrue("Choose Songs is working", condition);
    }

    /**
     * This test is to assert that songs are added correctly to the completedSongs list once they
     * are completed.
     */
     @Test
    public void completedSongListAdding() throws Exception {

        GameCreator testerGC = new GameCreator();
        testerGC.setSongList(songList);

        Song[] testCompleted = new Song[songList.size()];

        testerGC.chooseSong();
        int positionInArray = songList.indexOf(testerGC.getCurrentSong());
        testCompleted[positionInArray] = testerGC.getCurrentSong();
        testerGC.addSongToCompletedSongList();

        testerGC.chooseSong();
        positionInArray = songList.indexOf(testerGC.getCurrentSong());
        testCompleted[positionInArray] = testerGC.getCurrentSong();
        testerGC.addSongToCompletedSongList();

        testerGC.chooseSong();
        positionInArray = songList.indexOf(testerGC.getCurrentSong());
        testCompleted[positionInArray] = testerGC.getCurrentSong();
        testerGC.addSongToCompletedSongList();

        boolean condition = Arrays.equals(testerGC.getCompletedSongList(),testCompleted);

        assertTrue("Completed Song List is Working", condition);
    }

    /**
     * This test is to assert that the chooseSong() method isn't choosing a current song which is in
     * the list of completed songs and that the skipped song boolean is reset.
     *
     * @throws Exception if the test leads to one
     */
    @Test
    public void chooseSongNotSelectingCompleted() throws Exception {

        GameCreator testerGC = new GameCreator();
        testerGC.setSongList(songList);

        Song[] testCompleted = new Song[songList.size()];

        testerGC.chooseSong();
        int positionInArray = songList.indexOf(testerGC.getCurrentSong());
        testCompleted[positionInArray] = testerGC.getCurrentSong();
        testerGC.addSongToCompletedSongList();

        testerGC.chooseSong();
        positionInArray = songList.indexOf(testerGC.getCurrentSong());
        testCompleted[positionInArray] = testerGC.getCurrentSong();
        testerGC.addSongToCompletedSongList();

        testerGC.chooseSong();
        positionInArray = songList.indexOf(testerGC.getCurrentSong());
        testCompleted[positionInArray] = testerGC.getCurrentSong();
        testerGC.addSongToCompletedSongList();

        List<Song> containerList = Arrays.asList(testCompleted);

        testerGC.chooseSong();
        boolean condition = !containerList.contains(testerGC.getCurrentSong());

        assertTrue("Completed Songs not being selected", condition);
    }

    /**
     * This test is to assert that the chooseAndSkipPreviousSong() method is working. It asserts
     * that the skipped song has not been selected, that the current song isn't in the list of
     * completed songs and that the skipped song boolean is reset.
     *
     * @throws Exception if the test leads to one
     */
    @Test
    public void chooseAndSkipPreviousSongWorks() throws Exception {

        GameCreator testerGC = new GameCreator();
        testerGC.setSongList(songList);

        Song[] testCompleted = new Song[songList.size()];

        testerGC.chooseSong();
        int positionInArray = songList.indexOf(testerGC.getCurrentSong());
        testCompleted[positionInArray] = testerGC.getCurrentSong();
        testerGC.addSongToCompletedSongList();

        testerGC.chooseSong();
        positionInArray = songList.indexOf(testerGC.getCurrentSong());
        testCompleted[positionInArray] = testerGC.getCurrentSong();
        testerGC.addSongToCompletedSongList();

        testerGC.chooseSong();
        positionInArray = songList.indexOf(testerGC.getCurrentSong());
        testCompleted[positionInArray] = testerGC.getCurrentSong();
        testerGC.addSongToCompletedSongList();

        List<Song> containerList = Arrays.asList(testCompleted);

        testerGC.setSkippedSong(true);
        Song preSkippedSong = testerGC.getCurrentSong();

        testerGC.chooseAndSkipPreviousSong();
        assertTrue("Next song will not be skipped", !testerGC.getSkippedSong());

        boolean condition = !containerList.contains(testerGC.getCurrentSong());
        assertTrue("Completed Songs not being selected", condition);

        condition = !preSkippedSong.equals(testerGC.getCurrentSong());
        assertTrue("Skipped Song not being selected", condition);

    }
}
