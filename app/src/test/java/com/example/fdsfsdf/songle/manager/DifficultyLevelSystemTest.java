package com.example.fdsfsdf.songle.manager;

import com.example.fdsfsdf.songle.R;
import com.example.fdsfsdf.songle.objects.Song;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Local unit test for the DifficultyLevelSystem class, which will execute on the development
 * machine (host).
 */

public class DifficultyLevelSystemTest {

    /**
     * This method asserts that the setCurrentDifficulty(int setCurrentDifficulty) method is
     * setting the difficulty correctly where one has not been set before
     *
     * @throws Exception
     */
    @Test
    public void initialSetCurrentDifficultyTest() throws Exception {

        DifficultyLevelSystem testDLS = new DifficultyLevelSystem();
        int testCurrentDifficulty = 2;

        testDLS.setCurrentDifficulty(testCurrentDifficulty);

        assertEquals(testCurrentDifficulty, testDLS.getCurrentDifficulty());

    }

    /**
     * This method asserts that the setCurrentDifficulty(int setCurrentDifficulty) method is
     * setting the difficulty correctly where it is being increased on before
     *
     * @throws Exception
     */
    @Test
    public void increaseDifficultyTest() throws Exception {

        DifficultyLevelSystem testDLS = new DifficultyLevelSystem();

        int initialDifficulty = 2;
        int currentDifficulty = 4;
        int changeInDifficulty = 2;

        testDLS.setCurrentDifficulty(initialDifficulty);
        assertEquals(initialDifficulty, testDLS.getCurrentDifficulty());

        testDLS.setCurrentDifficulty(currentDifficulty);

        assertEquals(initialDifficulty, testDLS.getInitiallySetDifficulty());
        assertEquals(currentDifficulty, testDLS.getCurrentDifficulty());
        assertEquals(changeInDifficulty, testDLS.getChangeInDifficulty());

    }

    /**
     * This method asserts that the setCurrentDifficulty(int setCurrentDifficulty) method is
     * setting the difficulty correctly where it is being decreased on before
     *
     * @throws Exception
     */
    @Test
    public void decreaseDifficultyTest() throws Exception {

        DifficultyLevelSystem testDLS = new DifficultyLevelSystem();

        int initialDifficulty = 2;
        int currentDifficulty = 1;
        int changeInDifficulty = 1;

        testDLS.setCurrentDifficulty(initialDifficulty);
        assertEquals(initialDifficulty, testDLS.getCurrentDifficulty());

        testDLS.setCurrentDifficulty(currentDifficulty);

        assertEquals(initialDifficulty, testDLS.getInitiallySetDifficulty());
        assertEquals(currentDifficulty, testDLS.getCurrentDifficulty());
        assertEquals(changeInDifficulty, testDLS.getChangeInDifficulty());

    }

    /**
     * This method asserts that the setCurrentDifficulty(int setCurrentDifficulty) method is
     * setting the difficulty correctly where there is no change on before
     *
     * @throws Exception
     */
    @Test
    public void noChangeInDifficultyTest() throws Exception {

        DifficultyLevelSystem testDLS = new DifficultyLevelSystem();

        int initialDifficulty = 2;
        int currentDifficulty = 2;
        int changeInDifficulty = 0;

        testDLS.setCurrentDifficulty(initialDifficulty);
        assertEquals(initialDifficulty, testDLS.getCurrentDifficulty());

        testDLS.setCurrentDifficulty(currentDifficulty);

        assertEquals(initialDifficulty, testDLS.getInitiallySetDifficulty());
        assertEquals(currentDifficulty, testDLS.getCurrentDifficulty());
        assertEquals(changeInDifficulty, testDLS.getChangeInDifficulty());

    }

    /**
     * This method asserts that the setCurrentDifficulty(int setCurrentDifficulty) method is
     * setting the difficulty correctly where the change in the test is wrong
     *
     * @throws Exception
     */
    @Test
    public void changeIsWrongDifficultyTest() throws Exception {

        DifficultyLevelSystem testDLS = new DifficultyLevelSystem();

        int initialDifficulty = 2;
        int currentDifficulty = 4;
        int changeInDifficulty = 0;

        testDLS.setCurrentDifficulty(initialDifficulty);
        assertEquals(initialDifficulty, testDLS.getCurrentDifficulty());

        testDLS.setCurrentDifficulty(currentDifficulty);

        assertEquals(initialDifficulty, testDLS.getInitiallySetDifficulty());
        assertEquals(currentDifficulty, testDLS.getCurrentDifficulty());
        assertNotEquals(changeInDifficulty, testDLS.getChangeInDifficulty());

    }

    /**
     * This test is to assert that the correct map is going to be loaded based on the current
     * difficulty
     */
    @Test
    public void whichMapToLoad() throws Exception {

        DifficultyLevelSystem testDLS = new DifficultyLevelSystem();
        int initialDifficulty = 2;
        int whichMapToLoad = 4;

        testDLS.setCurrentDifficulty(initialDifficulty);

        assertEquals(testDLS.whichMapToLoad(), whichMapToLoad);

    }

    /**
     * This test is to assert that an error is thrown if the the correct map is not going to be loaded
     * based on the current difficulty
     */
    @Test(expected = Error.class)
    public void whichMapToLoadError() throws Exception {

        DifficultyLevelSystem testDLS = new DifficultyLevelSystem();
        int initialDifficulty = 7;

        testDLS.setCurrentDifficulty(initialDifficulty);

        try {
            testDLS.whichMapToLoad();
        } catch (Exception e) {
            String message = "Difficulty level not in range";
            assertEquals(message, e.getMessage());
            throw e;
        }
    }

    /**
     * This test is to assert that the correct string representing the current difficulty will be
     * drawn from resources
     */
    @Test
    public void getDifficultyString() throws Exception {

        DifficultyLevelSystem testDLS = new DifficultyLevelSystem();
        int initialDifficulty = 4;
        int expected = R.string.dHard;

        testDLS.setCurrentDifficulty(initialDifficulty);

        assertEquals(testDLS.getDifficultyString(), expected);
    }

    /**
     * This test is to assert that an error is thrown if the correct string representing the current
     * difficulty will not be drawn from resources
     */
    @Test(expected = Error.class)
    public void getDifficultyStringError() throws Exception {

        DifficultyLevelSystem testDLS = new DifficultyLevelSystem();
        int initialDifficulty = 7;

        testDLS.setCurrentDifficulty(initialDifficulty);

        try {
            testDLS.getDifficultyString();
        } catch (Error e) {
            String message = "Current difficulty not in range";
            assertEquals(message, e.getMessage());
            throw e;
        }
    }
}


