package com.example.fdsfsdf.songle.objects;


import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Local unit test for the Words class, which will execute on the development machine (host).
 */
public class WordsUnitTest {

    private List<List<String>> wordsList =
            Arrays.asList(
                    Arrays.asList("Tom", "John", "Mary", "Peter"),
                    Arrays.asList("Tom", "John", "Mary", "Peter"),
                    Arrays.asList("Tom", "John", "Mary", "Peter", "Mary", "Peter"),
                    Arrays.asList(""),
                    Arrays.asList("Tom", "John", "Mary", "Peter", "Mary", "Peter", "Mary", "Peter"),
                    Arrays.asList("Tom", "John", "Mary", "Peter", "Mary", "Peter"),
                    Arrays.asList("Tom", "John", "Mary"),
                    Arrays.asList("Tom", "John", "Mary", "Peter", "Mary")
            );

    private String[] line1 = {null, null, null, null};
    private String[] line2 = {null, null, null, "Peter"};
    private String[] line3 = {null, null, null, null, null, null,};
    private String[] line4 = {null};
    private String[] line5 = {null, "John", null, null, null, null, null, null};
    private String[] line6 = {null, null, null, null, null, null};
    private String[] line7 = {null, null, "Mary"};
    private String[] line8 = {null, null, null, null, null,};

    private List<String[]> accessedWordsList =
            Arrays.asList(line1, line2, line3, line4, line5, line6, line7, line8);

    private String[] lyricsNotAccessedData = {
            "???? ???? ???? ????",
            "???? ???? ???? ????",
            "???? ???? ???? ???? ???? ????",
            "",
            "???? ???? ???? ???? ???? ???? ???? ????",
            "???? ???? ???? ???? ???? ????",
            "???? ???? ????",
            "???? ???? ???? ???? ????"
    };

    private String[] lyricsAccessedData = {
            "???? ???? ???? ????",
            "???? ???? ???? Peter",
            "???? ???? ???? ???? ???? ????",
            "",
            "???? John ???? ???? ???? ???? ???? ????",
            "???? ???? ???? ???? ???? ????",
            "???? ???? Mary",
            "???? ???? ???? ???? ????"
    };


    @Test
    public void arraysAreSameSize() throws Exception {
        Words testData = new Words(wordsList);

        List<Integer> wordsSizes = new ArrayList<>();
        List<Integer> accessedWordsSizes = new ArrayList<>();

        List<List<String>> wordsTest = testData.getWordsList();
        for (int i = 0; i < wordsTest.size(); i++) {
            wordsSizes.add(wordsTest.get(i).size());
        }

        List<String[]> accessedWordsTest = testData.getAccessedWords();
        for (int i = 0; i < accessedWordsTest.size(); i++) {
            accessedWordsSizes.add(accessedWordsTest.get(i).length);
        }

        assertEquals(wordsSizes, accessedWordsSizes);

    }

    @Test
    public void lyricsIsCorrectNotAccessed() throws Exception {
        Words testData = new Words(wordsList);

        String [] printedLyrics = testData.printFormattedLyrics();

        boolean condition =
                Arrays.equals(printedLyrics, lyricsNotAccessedData);

        assertTrue("Lyrics Un-accessed is Correct", condition);

    }

    @Test
    public void lyricsIsCorrectAccessed() throws Exception {
        Words testData = new Words(wordsList);
        testData.setAccessedWords(accessedWordsList);

        String [] printedLyrics = testData.printFormattedLyrics();

        boolean condition =
                Arrays.equals(printedLyrics, lyricsAccessedData);

        assertTrue("Lyrics Accessed is Correct", condition);

    }
}