package com.example.fdsfsdf.songle.objects;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Local unit test for the Placemark class, which will execute on the development machine (host).
 */
public class SongTest {

    private Song testSong = new Song("01", "Queen", "Bohemian Rhapsody",
            "http://www.inf.ed.ac.uk/teaching/courses/selp/data/songs/01/words.txt");

    private List<List<String>> wordsList =
            Arrays.asList(
                    Arrays.asList("Tom", "John", "Mary", "Peter"),
                    Arrays.asList("Tom", "John", "Mary", "Peter"),
                    Arrays.asList("Tom", "John", "Mary", "Peter", "Mary", "Peter"),
                    Arrays.asList(""),
                    Arrays.asList("Tom", "John", "Mary", "Peter", "Mary", "Peter", "Mary", "Peter"),
                    Arrays.asList("Tom", "John", "Mary", "Peter", "Mary", "Peter"),
                    Arrays.asList("Tom", "John", "Mary"),
                    Arrays.asList("Tom", "John", "Mary", "Peter", "Mary")
            );

    private Words testWords = new Words(wordsList);

    private List<String> testPreviousGuesses = Arrays.asList("Tom", "John", "Mary", "Peter");

    private List<Placemark> testPlacemarks = Arrays.asList(
            new Placemark("4:1", "boring", "#boring", new Point("-3.1896948527782767,55.945066395849906,0")),
            new Placemark("7:4", "notboring", "#notboring", new Point("-3.191022482076821,55.945409920152805,0")),
            new Placemark("3:2", "interesting", "#interesting", new Point("-3.1854195299326578,55.94619505458477,0")),
            new Placemark("3:1", "veryinteresting", "#veryinteresting", new Point("-3.187150971691298,55.944928787507585,0")),
            new Placemark("6:4", "unclassified", "#unclassified", new Point("-3.186669178092598,55.94444913310533,0"))
    );

    private Map testMap1 = new Map();

    @Before
    public void setUp() {
        for (Placemark testPlacemark : testPlacemarks) {
            testMap1.add(testPlacemark);
        }

        testSong.setMapSelectNum(1);
        testSong.setMap(testMap1);
        testSong.getMap().getPlacemark(1).setPlacemarkRemoved(true);

        for (String prevGuess : testPreviousGuesses) {
            testSong.setPreviousGuess(prevGuess);
        }

        testSong.setWords(testWords);
        testSong.getWords().getWord("3:1", true);
        testSong.getWords().getWord("6:4", true);
    }

    /**
     * Method is used to test that the word which has been accessed through the getWord method in
     * Words is correct.
     *
     * The use of the Song object allows simultaneous testing of the Words and Placemarker classes.
     *
     * @throws Exception
     */
    @Test
    public void getWordMethodWorksWithPlacemarks() throws Exception {

        String placemarkerName1 = testPlacemarks.get(2).getName();
        String placemarkerName2 = testPlacemarks.get(3).getName();

        String testWord1 = testSong.getWords().getWord(placemarkerName1, true);
        String testWord2 = testSong.getWords().getWord(placemarkerName2, true);

        assertEquals(testWord1, "John");
        assertEquals(testWord2, "Tom");
    }

    /**
     * Method principally tests the method resetSong() but in doing so test numerous dependent class
     * methods as well.
     */
    @Test
    public void resetSongTest() throws Exception {

        testSong.resetSong();

        boolean wordsReset = true;
        List<String[]> testAccessedWords = testSong.getWords().getAccessedWords();

        for (String[] accessedWordLine : testAccessedWords) {
            boolean unAccessedLine = true;
            for (String accessedWord : accessedWordLine) {
                if (accessedWord != null) {
                    unAccessedLine = false;
                    break;
                }
            }
            if (!unAccessedLine) {
                wordsReset = false;
                break;
            }
        }

        assertTrue("Words has been reset", wordsReset);

        boolean prevGuessesReset = testSong.getPreviousGuesses().isEmpty();

        assertTrue("PreviousGuesses has been reset", prevGuessesReset);

        boolean mapsReset = true;
        for (int i = 1; i <= 5; i++) {
            testSong.setMapSelectNum(i);

            if (testSong.getMap() != null) {
                List<Placemark> placemarkCheck = testSong.getMap().getPlacemarks();

                for (Placemark current : placemarkCheck) {
                    mapsReset = mapsReset && !current.isPlacemarkRemoved();
                }
            }
        }

        assertTrue("Maps have been reset", mapsReset);
    }

}

