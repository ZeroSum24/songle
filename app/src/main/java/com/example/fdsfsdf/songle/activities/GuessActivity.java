package com.example.fdsfsdf.songle.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.fdsfsdf.songle.R;
import com.example.fdsfsdf.songle.manager.AlertDialogManager;
import com.example.fdsfsdf.songle.manager.GameCreator;

import java.util.List;

public class GuessActivity extends AppCompatActivity {

    private int amountOfGuesses;
    private EditText editTextBox;
    private TextView previousGuessesTextView;

    private Button lyricsButton;
    private Button mapButton;
    private Button skipSong;
    private RelativeLayout settingsMenu;
    private View congratsOverlay;
    public static final String EXTRA_MESSAGE = "com.example.songle.GUESS_OVERLAY_ACCESS";

    private GameCreator accessGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guess);

        accessGame = (GameCreator) getApplicationContext();

        displayPointsSystem();
        generalActivityIntents();
        guessNormFunctions();
    }

    @Override
    protected void onStart() {
        super.onStart();
        displayPointsSystem();
    }

    /**
     * Method checks the points which the user has collected and displays them on the activity.
     */
    private void displayPointsSystem() {

        //Initialises the Views here
        TextView setCurrentPoints = findViewById(R.id.currentPoints);
        TextView setTotalPoints = findViewById(R.id.totalPoints);

        //Accesses the data needed to display the views
        String currentPoints = Integer.toString(accessGame.getPointsSystem().getCurrentPoints());
        String totalPoints = Integer.toString(accessGame.getPointsSystem().getTotalPoints());

        //Sets the views here
        setCurrentPoints.setText(currentPoints);
        setTotalPoints.setText(totalPoints);

    }

    /**
     * Method establishes the normal functions of the guess activity.
     * <p>
     * It sets up the editText box so the user can input their guess and have it checked for
     * correctness. If it is correct it calls the congratulations overlay, otherwise it displays the
     * text as a previous guess.
     */
    private void guessNormFunctions() {
        //GUESS NORM FUNCTIONS - ACCEPT GUESSES AND ADD TO PREVIOUS GUESSES

        ImageButton enterGuessesButton;
        congratsOverlay = findViewById(R.id.bubble_guess_congrats);
        previousGuessesTextView = findViewById(R.id.previousGuesses);


        if (!accessGame.getCurrentSong().getPreviousGuesses().isEmpty()) {
            List<String> previousGuesses = accessGame.getCurrentSong().getPreviousGuesses();
            for (int i = previousGuesses.size() - 1; i >= 0; i--) {
                previousGuessesTextView.append(previousGuesses.get(i));
            }
        }

        editTextBox = findViewById(R.id.enterGuesses);
        enterGuessesButton = findViewById(R.id.enterGuessButton);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN); //makes the keyboard not pop up on guess access
        Log.e("GuessNormFunctions", "CurrentSong name: " + accessGame.getCurrentSong().getTitle());

        enterGuessesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                String guessesEntry = editTextBox.getText().toString();
                String guess = guessesEntry.replaceAll("[\\W]", "");
                editTextBox.setText("");

                //COMPLETED edit so that punctuation does not matter
                String songName = accessGame.getCurrentSong().getTitle();
                songName = songName.replaceAll("[\\W]", "");

                if (!guess.toLowerCase().equals(songName.toLowerCase())) {
                    //replace with correct song name - functionality

                    accessGame.getCurrentSong().setPreviousGuess(guessesEntry + "\n");       //adds new guess to entry
                    amountOfGuesses = accessGame.getCurrentSong().getPreviousGuesses().size();

                    previousGuessesTextView.setText(guessesEntry + "\n"
                            + previousGuessesTextView.getText());

                    if (amountOfGuesses >= 5) {
                        skipSong.setVisibility(View.VISIBLE);
                    }
                } else {
                    amountOfGuesses = 1;
                    congratsOverlayFunctions();
                    accessGame.addSongToCompletedSongList();
                    accessGame.getPointsSystem().updateTotalPoints();
                    displayPointsSystem();

                }
            }
        });
    }

    /**
     * Method establishes the congratulations overlay for guessing a song.
     * <p>
     * It establishes the intents for the buttons it uses and makes the overlay visible. It updates
     * the overlay with the right amount of points and the song name.The Overlay also allows the
     * user to quit the game and updates the users score to be submitted by the MainMenuActivity.
     * That is if Submit Score is pressed.
     * <p>
     * The Overlay also handles the case where the game has been completed. Allowing users to start
     * the game afresh if they wish.
     * <p>
     * Called by guessNormFunctions()
     */
    private void congratsOverlayFunctions() {

        // Initialise views here

        TextView fixedGuessText = findViewById(R.id.fixedEnterGuesses);
        TextView setNewTotalPoints = findViewById(R.id.bGTotalSongPoints);
        TextView setYourSongIs = findViewById(R.id.bGYourSongIs);
        TextView setDifficultyLevel = findViewById(R.id.bGDifficultyLevelText);

        congratsOverlay.setVisibility(View.VISIBLE);
        fixedGuessText.setVisibility(View.INVISIBLE);


        //Access data for Congrats Overlay here
        String currentSong = accessGame.getCurrentSong().getTitle();
        String difficultyLevel = getString(accessGame.getDifficultyLevelSystem()
                .getDifficultyString());

        int currentDifficulty = accessGame.getDifficultyLevelSystem().getCurrentDifficulty();
        int initialDifficulty = accessGame.getDifficultyLevelSystem().getInitiallySetDifficulty();
        int changeInDifficulty = accessGame.getDifficultyLevelSystem().getChangeInDifficulty();

        accessGame.getPointsSystem().addToGuessNewTPoints(amountOfGuesses);
        accessGame.getPointsSystem().difficultyBonusNewTPoints(currentDifficulty, changeInDifficulty,
                initialDifficulty);
        String totalSongPoints = Integer.toString(accessGame.getPointsSystem().getTotalSongPoints());

        //Sets View content here
        setNewTotalPoints.setText(totalSongPoints);
        setDifficultyLevel.setText(difficultyLevel);
        setYourSongIs.setText(currentSong);

        //Buttons used for Overlay

        Button oPlayAgainButton;
        Button oViewLeaderboard;
        Button oViewCompletedSongsButton;

        oPlayAgainButton = findViewById(R.id.bGPlayAgainButton);
        oViewLeaderboard = findViewById(R.id.bGSubmitScoreAndLeaderBoard);
        oViewCompletedSongsButton = findViewById(R.id.bGCompletedSongs);


        oPlayAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accessGame.setNewSongNeeded(true);

                if (accessGame.checkIfCompletedSongListIsFull()) {
                    //Case for when the game has been completed and the user wishes to continue playing

                    AlertDialogManager alert = new AlertDialogManager();
                    alert.showAlertDialog(GuessActivity.this, getString(R.string.gCompletedGameHeader),
                            getString(R.string.gCompletedGameMessage),
                            getString(R.string.gCompletedGameButton),
                            true, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    accessGame.resetGameIfFinished();
                                    startActivity(new Intent(GuessActivity.this, DifficultyActivity.class));
                                }
                            });
                } else {
                    startActivity(new Intent(GuessActivity.this, DifficultyActivity.class));
                }
            }
        });

        oViewLeaderboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialogManager alert = new AlertDialogManager();
                alert.showAlertDialog(GuessActivity.this, null,
                        getString(R.string.gFinishGameHeader), getString(R.string.gFinishGameButton),
                        true, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                int pointsToSubmit = accessGame.getPointsSystem().pointsToSubmit();
                                accessGame.setUsersScoreToSubmit(pointsToSubmit);
                                startActivity(new Intent(GuessActivity.this, MainMenuActivity.class));
                            }
                        });
            }
        });

        oViewCompletedSongsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accessGame.setNewSongNeeded(true);
                Intent intent = new Intent(GuessActivity.this, CompletedSongsActivity.class);
                intent.putExtra(EXTRA_MESSAGE, true);
                startActivity(intent);
            }
        });

    }

    /**
     * Method manages the two intent buttons (lyrics and map) which
     * governs which activity is accessed (button name respective). It also governs the settings
     * button.
     * <p>
     * It also highlights the chosen button to show the user which has been clicked.
     * <p>
     * The method also initiates the button for the user to skip a song
     */
    private void generalActivityIntents() {
        //Sets up the buttons and intents of activity

        ImageButton settingsButton;

        lyricsButton = findViewById(R.id.leftButton);
        mapButton = findViewById(R.id.rightButton);
        settingsButton = findViewById(R.id.settingsButton);
        settingsMenu = findViewById(R.id.settingsMenu);

        // Resetting text for left and right buttons
        lyricsButton.setText(R.string.cLyrics);
        mapButton.setText(R.string.cMap);

        // Watching clicks on both lyrics and right click buttons
        lyricsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lyricsButton.setTextColor(getColor(R.color.colorIcons));
                startActivity(new Intent(GuessActivity.this, LyricsActivity.class));
            }
        });
        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapButton.setTextColor(getColor(R.color.colorIcons));
                startActivity(new Intent(GuessActivity.this, MapsActivity.class));
            }
        });

        settingsButton.setZ(9);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int visibility = settingsMenu.getVisibility();
                if (visibility == View.INVISIBLE) {
                    settingsMenu.setZ(8);
                    settingsMenu.setVisibility(View.VISIBLE);
                    settingsIntentLinks();
                } else if (visibility == View.VISIBLE) {
                    settingsMenu.setVisibility(View.INVISIBLE);
                }
            }
        });

        //Skip Song Button

        skipSong = findViewById(R.id.gSkipSong);

        skipSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accessGame.setSkippedSong(true);
                accessGame.setNewSongNeeded(true);
                startActivity(new Intent(GuessActivity.this, DifficultyActivity.class));
            }
        });
    }

    /**
     * Method manages the buttons (change difficulty, how to play, view completed) which
     * governs which activity is accessed (button name respective).
     * <p>
     * It displays an alert if the user wishes to change their difficulty as this may
     * effect their score via the Difficulty Bonus.
     * <p>
     * Called by generalActivityIntents()
     */
    private void settingsIntentLinks() {

        Button changeDifficultyButton;
        Button howToPlayButton;
        Button viewCompletedSongsButton;

        changeDifficultyButton = findViewById(R.id.sChangeDifficultyButton);
        howToPlayButton = findViewById(R.id.sHowToPlayButton);
        viewCompletedSongsButton = findViewById(R.id.sViewCompletedSongsButton);

        changeDifficultyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialogManager alert = new AlertDialogManager();
                alert.showAlertDialog(GuessActivity.this, null,
                        getString(R.string.sAlertChangeDifficultyMessage),
                        getString(R.string.sAlertChangeDifficultyButton),
                        true, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                settingsMenu.setVisibility(View.INVISIBLE);
                                startActivity(new Intent(GuessActivity.this, DifficultyActivity.class));

                            }
                        });
            }
        });

        howToPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsMenu.setVisibility(View.INVISIBLE);
                startActivity(new Intent(GuessActivity.this, HowToPlayActivity.class));
            }
        });

        viewCompletedSongsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsMenu.setVisibility(View.INVISIBLE);
                startActivity(new Intent(GuessActivity.this, CompletedSongsActivity.class));
            }
        });

    }

    /**
     * Overrides method to allow prevent the user accessing the last activity and allowing them the
     * option to quit the session. It also updates the users score to be submitted by the
     * Main Menu Activity.
     */
    @Override
    public void onBackPressed() {
        AlertDialogManager alert = new AlertDialogManager();
        alert.showAlertDialog(GuessActivity.this, null,
                getString(R.string.cQuitAlertHeader), getString(R.string.cQuitAlertButton),
                true, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int pointsToSubmit = accessGame.getPointsSystem().pointsToSubmit();
                        accessGame.setUsersScoreToSubmit(pointsToSubmit);

                        startActivity(new Intent(GuessActivity.this, MainMenuActivity.class));
                    }
                });
    }

    /**
     * Overrides method to allow the settings menu to close if anywhere outside it is touched
     * after it is opened. It also prevents other buttons being access whilst the guessed song
     * overlay is displayed
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        //Used to hide settings menu when screen is touched anywhere but menu

        //Close on touch outside for settings
        Rect viewRect = new Rect();
        settingsMenu.getGlobalVisibleRect(viewRect);
        boolean isClickOutsideSettingsMenu = !viewRect.contains((int) ev.getRawX(), (int) ev.getRawY());
        if (isClickOutsideSettingsMenu && settingsMenu.getVisibility() == View.VISIBLE) {
            settingsMenu.setVisibility(View.INVISIBLE);
            return true;
        }

        //Don't touch outside for Map Congrats Overlay
        Rect congratsRect = new Rect();
        congratsOverlay.getGlobalVisibleRect(congratsRect);
        boolean isClickOutsideOverlay = !congratsRect.contains((int) ev.getRawX(), (int) ev.getRawY());
        if (isClickOutsideOverlay && congratsOverlay.getVisibility() == View.VISIBLE) {
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }
}
