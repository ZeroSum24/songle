/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.fdsfsdf.songle.utilities.Network;

import android.util.Log;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * These utilities will be used to communicate with the network.
 */
public class BuildDownloadURL {

    final static String BASE_DATA_URL =
            "http://www.inf.ed.ac.uk/teaching/courses/selp/data/songs/";

    /**
     * Builds the URL for the data to be downloaded by the app.
     *
     * @param file       This will indicate which fileType is to be downloaded
     * @param songNumber If particular song data is to be downloaded this will indicate which song
     * @param mapNumber This indicates the map to be loaded
     * @return The URL to be used to download the data.
     */

    public static URL buildUrl(String file, String songNumber, String mapNumber) {

        String builtUri = BASE_DATA_URL;

        if (songNumber != null) {
            //If there is a songNumber it should be added to the path
            builtUri += songNumber;
        }
        if (mapNumber != null && file.equals("map_.kml")) {
            Log.e("BuildDownloadURL", "Map number: "+ mapNumber);
            //If the map number is not null, a map has to be loaded and added to the path
            String[] splitForNum = file.split("_");
            file = splitForNum[0] + mapNumber + splitForNum[1];
        }
        if (file.equals("songs.xml")) {
            builtUri += file;
        } else {
            builtUri += "/" + file;
        }

        URL url = null;
        try {
            url = new URL(builtUri);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        Log.e("BuildDownloadURL", "Build contents: "+ url.toString());
        return url;
    }
}