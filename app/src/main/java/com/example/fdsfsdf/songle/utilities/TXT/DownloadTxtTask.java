package com.example.fdsfsdf.songle.utilities.TXT;

import android.os.AsyncTask;
import android.util.Log;

import com.example.fdsfsdf.songle.objects.Words;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class DownloadTxtTask extends AsyncTask<URL, Void, Words> { //changed class name for some reason

    @Override
    protected Words doInBackground(URL... urls) {
        try {
            return loadTxtFromNetwork(urls[0]);
        } catch (IOException e) {
            throw new Error("Unable to load content. Check your network connection");
        } catch (XmlPullParserException e) {
            throw new Error("Error parsing txt Map");
        }
    }

    @Override
    protected void onPostExecute(Words result) {
        // Do something with result
        Log.e("DownloadTxt", "WordList " + "COMPLETED");
        Log.e("DownloadTxt", "WordList 1stItem: " + result.getWord("1:1", false));
    }

    private Words loadTxtFromNetwork(URL url) throws XmlPullParserException, IOException {
        Words words;
        try (InputStream stream = downloadUrl(url)) {
            // Do something with stream e.g. parse as XML, build result

            /* parses the words/lyrics into a List structure like String[line][words] and save as class Words
            */
            ParsingTxtTask songWordsParser = new ParsingTxtTask();
            List wordsList = songWordsParser.readDocument(stream);
            words = new Words(wordsList);

            Log.e("isDWordsListEmpty", "isDWordsListEmpty: " + (wordsList.isEmpty()));
        }
        return words;
    }

    // Given a string representation of a URL, sets up a connection and gets
    // an input stream.
    private InputStream downloadUrl(URL url) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        // Also available: HttpsURLConnection
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // Starts the query
        conn.connect();
        return conn.getInputStream();
    }


}


