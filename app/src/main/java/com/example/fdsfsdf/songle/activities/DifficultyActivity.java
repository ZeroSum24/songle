package com.example.fdsfsdf.songle.activities;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.example.fdsfsdf.songle.R;
import com.example.fdsfsdf.songle.manager.AlertDialogManager;
import com.example.fdsfsdf.songle.manager.GameCreator;
import com.example.fdsfsdf.songle.utilities.Network.BuildDownloadURL;
import com.example.fdsfsdf.songle.utilities.KML.DownloadKmlTask;
import com.example.fdsfsdf.songle.utilities.TXT.DownloadTxtTask;
import com.example.fdsfsdf.songle.utilities.Network.NetworkReceiver;

import java.net.URL;
import java.util.concurrent.ExecutionException;

public class DifficultyActivity extends AppCompatActivity {

    private GameCreator accessGame;

    private Button veryEasyButton;
    private Button easyButton;
    private Button normalButton;
    private Button hardButton;
    private Button veryHardButton;
    private RelativeLayout settingsMenu;
    private ProgressBar downloadProgressBar;

    private final int Difficulty_VERY_EASY = 1;
    private final int Difficulty_EASY = 2;
    private final int Difficulty_NORMAL = 3;
    private final int Difficulty_HARD = 4;
    private final int Difficulty_VERY_HARD = 5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_difficulty_screen);

        //Initialises Difficulty System
        accessGame = ((GameCreator) this.getApplication());

        setSongOrChangeDifficulty(); //Must be top in order
        generalActivityIntents();
        downloadLyrics();

        showCurrentDifficulty();
    }

    /**
     * Method sets the song to be used for the game session. Calls different methods whether the
     * song is the first of the session, the previous song has been skipped or it is time for a new
     * song.
     *
     * It sets the current song for the session in GameCreator.
     */
    private void setSongOrChangeDifficulty() {
        boolean newSongNeeded = accessGame.getNewSongNeeded();
        boolean previousSongSkipped = accessGame.getSkippedSong();
        boolean firstSongOfSession = false;

        if (accessGame.getCurrentSong() == null) {
            firstSongOfSession = true;
        }

        if (newSongNeeded) {
            if (previousSongSkipped) {
                accessGame.chooseAndSkipPreviousSong();
            } else {
                accessGame.chooseSong();
            }
        } else if (firstSongOfSession) {
            accessGame.chooseSong();
        }
    }

    /**
     * Method manages the five difficulty buttons (very easy, easy, normal, hard, very hard) which
     * governs which difficulty is set (button name respective). The map activity is called
     * after one of these buttons is clicked. It also governs the settings button.
     *
     * The chosen button (barring settings) is highlighted to show the user which has been clicked.
     *
     * Sets the current difficulty in Game Creator.
     */
    private void generalActivityIntents() {

        ImageButton settingsButton = findViewById(R.id.settingsButton);

        veryEasyButton = findViewById(R.id.dVeryEasyButton);
        easyButton = findViewById(R.id.dEasyButton);
        normalButton = findViewById(R.id.dNormalButton);
        hardButton = findViewById(R.id.dHardButton);
        veryHardButton = findViewById(R.id.dVeryHardButton);
        settingsMenu = findViewById(R.id.settingsMenu);

        View.OnClickListener buttonClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressBar();

                veryEasyButton.setTextColor(getColor(R.color.colorPrimary_text));
                easyButton.setTextColor(getColor(R.color.colorPrimary_text));
                normalButton.setTextColor(getColor(R.color.colorPrimary_text));
                hardButton.setTextColor(getColor(R.color.colorPrimary_text));
                veryHardButton.setTextColor(getColor(R.color.colorPrimary_text));

                Intent intent = new Intent(DifficultyActivity.this, MapsActivity.class);

                switch (v.getId()) {
                    case R.id.dVeryEasyButton:
                        veryEasyButton.setTextColor(getColor(R.color.colorIcons));
                        accessGame.getDifficultyLevelSystem().setCurrentDifficulty(Difficulty_VERY_EASY);
                        break;
                    case R.id.dEasyButton:
                        easyButton.setTextColor(getColor(R.color.colorIcons));
                        accessGame.getDifficultyLevelSystem().setCurrentDifficulty(Difficulty_EASY);
                        break;
                    case R.id.dNormalButton:
                        normalButton.setTextColor(getColor(R.color.colorIcons));
                        accessGame.getDifficultyLevelSystem().setCurrentDifficulty(Difficulty_NORMAL);
                        break;
                    case R.id.dHardButton:
                        hardButton.setTextColor(getColor(R.color.colorIcons));
                        accessGame.getDifficultyLevelSystem().setCurrentDifficulty(Difficulty_HARD);
                        break;
                    case R.id.dVeryHardButton:
                        veryHardButton.setTextColor(getColor(R.color.colorIcons));
                        accessGame.getDifficultyLevelSystem().setCurrentDifficulty(Difficulty_VERY_HARD);
                        break;
                }

                int whichMapToLoad = accessGame.getDifficultyLevelSystem().whichMapToLoad();
                accessGame.getCurrentSong().setMapSelectNum(whichMapToLoad);
                downloadMap();
                startActivity(intent);
            }
        };

        veryEasyButton.setOnClickListener(buttonClickListener);
        easyButton.setOnClickListener(buttonClickListener);
        normalButton.setOnClickListener(buttonClickListener);
        hardButton.setOnClickListener(buttonClickListener);
        veryHardButton.setOnClickListener(buttonClickListener);

        settingsButton.setZ(9); //Sets the settingsButton and settingsMenu higher than all else
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int visibility = settingsMenu.getVisibility();
                if (visibility == View.INVISIBLE) {
                    settingsMenu.setZ(8);
                    settingsMenu.setVisibility(View.VISIBLE);
                    settingsIntentLinks();
                } else if (visibility == View.VISIBLE) {
                    settingsMenu.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    /**
     * Method downloads the lyrics for the song if this has not previously been downloaded. It also
     * checks if internet access is available to do this and if it is not, it displays a Progress
     * Bar whilst the internet connection is established
     *
     * @exception NullPointerException on uncompleted map download.
     * @exception InterruptedException if the download is interrupted.
     * @exception ExecutionException is thrown if the map is called after the download is interrupted.
     *
     * Adds the lyrics to the current song in Game Creator.
     */
    private void downloadLyrics() {

        boolean lyricsInstDownloaded = accessGame.getCurrentSong().getWords() == null;

        if (lyricsInstDownloaded) { //need to download the lyrics, otherwise do nothing
            if (isInternetEnabled()) { //download the lyrics
                URL[] urls = new URL[1];

                URL songMapDLUrl = BuildDownloadURL.buildUrl("lyrics.txt",
                        accessGame.getCurrentSong().getNumber(), null);
                urls[0] = songMapDLUrl;

                try {
                    DownloadTxtTask downloadTxtTask = new DownloadTxtTask();
                    accessGame.getCurrentSong().setWords(downloadTxtTask.execute(urls).get());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    System.out.print("Download did not complete");
                }
            } else { //show progress bar and call check Internet Connection Snackbar
                showProgressBar();
                checkInternetConnection(true);
            }
        }
    }

    /**
     * Method downloads a map of a song if this has not previously been downloaded. It also
     * checks if internet access is available to do this and if it is not, it displays a Progress
     * Bar whilst the internet connection is established
     *
     * @exception NullPointerException on uncompleted map download
     * @exception InterruptedException if the download is interrupted
     * @exception ExecutionException is thrown if the map is called after the download is interrupted
     *
     * Sets the map in the map structure in the Song class, for the currentSong in Game Creator.
     *
     * Called in generalActivityIntents()
     */
    private void downloadMap() {
        //download the correct map
        boolean currentMapIsntDownloaded = accessGame.getCurrentSong().getMap() == null;

        if (currentMapIsntDownloaded) { //go ahead with download, else do nothing
            if (isInternetEnabled()) { //download the map

                URL[] urls = new URL[1];

                URL songMapDLUrl = BuildDownloadURL.buildUrl("map_.kml",
                        accessGame.getCurrentSong().getNumber(), Integer.toString(accessGame.getDifficultyLevelSystem().whichMapToLoad()));
                urls[0] = songMapDLUrl;

                try {
                    DownloadKmlTask downloadKmlSongs = new DownloadKmlTask();
                    accessGame.getCurrentSong().setMap(downloadKmlSongs.execute(urls).get());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    System.out.print("Download did not complete");
                }

            } else { //show the progress bar and download the map
                showProgressBar();
                checkInternetConnection(false);
            }
        }
    }

    /**
     * Method displays a SnackBar to allow the user to recheck the internet connection and download
     * songs.xml. The SnackBar hides the ProgressBar if the internet connection has been
     * established.
     *
     * The method is repeatedly called if the internet is not established, the progress bar will not
     * be dismissed until it is. The swipe-to-dismiss functionality has been disabled to prevent
     * the user being stuck on a permanent ProgressBar
     *
     * Called by downloadMap() and downloadLyrics()
     *
     * @param isLyricsMethodPass Used to check if lyrics are to be downloaded or a map
     */
    private void checkInternetConnection(final boolean isLyricsMethodPass) {

        //Standard code for SnackBar updated to relate to internet Connection
        Snackbar internetSnackBar = Snackbar.make(findViewById(R.id.dCoordinatorLayout),
                R.string.noInternet, Snackbar.LENGTH_INDEFINITE);
        final View snackBarView = internetSnackBar.getView();
        internetSnackBar.setAction(R.string.retry, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideProgressBar();
                if (isLyricsMethodPass) {
                    downloadLyrics();
                } else {
                    downloadMap();
                }
            }
        });
        internetSnackBar.show();

        //Code to stop swipe ability of SnackBar and avoid functionality
        snackBarView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                snackBarView.getViewTreeObserver().removeOnPreDrawListener(this);
                ((CoordinatorLayout.LayoutParams) snackBarView.getLayoutParams()).setBehavior(null);
                return true;
            }
        });
    }

    /**
     * Method checks whether the internet connection has been established by accessing the Network
     * Receiver.
     *
     * Sets songs.xml as the songList variable in Game Creator.
     *
     * Called by checkInternetConnection()
     */
    private boolean isInternetEnabled() {
        //Method to check if the internet connection is enabled
        NetworkReceiver receiver = new NetworkReceiver();
        return receiver.isConnectionDetected(getApplicationContext());
    }

    /**
     * Method hides the progress bar and displays the difficulty buttons to enable game progress.
     *
     * Called by checkInternetConnection()
     */
    private void hideProgressBar() {
        downloadProgressBar = findViewById(R.id.dProgressBar);

        downloadProgressBar.setVisibility(View.INVISIBLE);
        veryEasyButton.setVisibility(View.VISIBLE);
        easyButton.setVisibility(View.VISIBLE);
        normalButton.setVisibility(View.VISIBLE);
        hardButton.setVisibility(View.VISIBLE);
        veryHardButton.setVisibility(View.VISIBLE);

    }

    /**
     * Method displays the progress bar and hides the difficulty buttons to prevent game progress.
     *
     * Called by downloadMap() and downloadLyrics()
     */
    private void showProgressBar() {
        downloadProgressBar = findViewById(R.id.dProgressBar);

        downloadProgressBar.setVisibility(View.VISIBLE);
        veryEasyButton.setVisibility(View.INVISIBLE);
        easyButton.setVisibility(View.INVISIBLE);
        normalButton.setVisibility(View.INVISIBLE);
        hardButton.setVisibility(View.INVISIBLE);
        veryHardButton.setVisibility(View.INVISIBLE);
    }

    /**
     * Method sets the text color to indicate the current difficulty. On each button click,
     * the colour of the current button text changes and all the other button colors reset.
     *
     * Called by generalActivityIntents()
     */
    private void showCurrentDifficulty() {

        int currentDifficulty = accessGame.getDifficultyLevelSystem().getCurrentDifficulty();

        switch (currentDifficulty) {
            case 0:
                break;
            case Difficulty_VERY_EASY:
                veryEasyButton.setTextColor(getColor(R.color.colorIcons));
                break;
            case Difficulty_EASY:
                easyButton.setTextColor(getColor(R.color.colorIcons));
                break;
            case Difficulty_NORMAL:
                normalButton.setTextColor(getColor(R.color.colorIcons));
                break;
            case Difficulty_HARD:
                hardButton.setTextColor(getColor(R.color.colorIcons));
                break;
            case Difficulty_VERY_HARD:
                veryHardButton.setTextColor(getColor(R.color.colorIcons));
                break;
            default:
                throw new Error("Current difficulty not in range");
        }
    }

    /**
     * Method manages the buttons (change difficulty, how to play, view completed) which
     * governs which activity is accessed (button name respective).
     *
     * Called by generalActivityIntents()
     */
    private void settingsIntentLinks() {

        Button changeDifficultyButton;
        Button howToPlayButton;
        Button viewCompletedSongsButton;

        changeDifficultyButton = findViewById(R.id.sChangeDifficultyButton);
        howToPlayButton = findViewById(R.id.sHowToPlayButton);
        viewCompletedSongsButton = findViewById(R.id.sViewCompletedSongsButton);

        changeDifficultyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsMenu.setVisibility(View.INVISIBLE);
            }
        });

        howToPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsMenu.setVisibility(View.INVISIBLE);
                startActivity(new Intent(DifficultyActivity.this, HowToPlayActivity.class));
            }
        });

        viewCompletedSongsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsMenu.setVisibility(View.INVISIBLE);
                startActivity(new Intent(DifficultyActivity.this, CompletedSongsActivity.class));
            }
        });

    }

    /**
     * Overrides method to allow prevent the user accessing the last activity and allowing them the
     * option to quit the session. It also updates the users score to be submitted by the
     * Main Menu Activity.
     */
    @Override
    public void onBackPressed() {
        AlertDialogManager alert = new AlertDialogManager();
        alert.showAlertDialog(DifficultyActivity.this, null,
                getString(R.string.cQuitAlertHeader), getString(R.string.cQuitAlertButton),
                true, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pointsToSubmit = accessGame.getPointsSystem().pointsToSubmit();
                        accessGame.setUsersScoreToSubmit(pointsToSubmit);

                        startActivity(new Intent(DifficultyActivity.this, MainMenuActivity.class));
                    }
                });
    }

    /**
     * Overrides method to close the settings menu if anywhere outside the settings menu is touched
     * after it is opened.
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        //Used to hide settings menu when screen is touched anywhere but menu
        Rect viewRect = new Rect();
        settingsMenu.getGlobalVisibleRect(viewRect);
        boolean isClickInSettingsMenu = !viewRect.contains((int) ev.getRawX(), (int) ev.getRawY());
        if (isClickInSettingsMenu && settingsMenu.getVisibility() == View.VISIBLE) {
            settingsMenu.setVisibility(View.INVISIBLE);
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }
}
