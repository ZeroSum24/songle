package com.example.fdsfsdf.songle.objects;

import android.support.annotation.VisibleForTesting;

import java.util.ArrayList;
import java.util.List;

public class Map{

    private final List<Placemark> placemarks;

    public Map() {
        this.placemarks= new ArrayList<>();
    }

    public void add(Placemark placemark) {
        placemarks.add(placemark);
    }

    public Placemark getPlacemark(int i) {
        return placemarks.get(i);
    }
    public int mapSize() {
        return placemarks.size();
    }

    /**
     * Method is used to reset the placemarks to null in the case that the
     * game is being reset.
     */
    void resetPlaceMarkers() {

        for (Placemark current: placemarks) {

            current.setPlacemarkRemoved(false);
        }
    }

    @VisibleForTesting
    public List<Placemark> getPlacemarks() {
        return placemarks;
    }
}

