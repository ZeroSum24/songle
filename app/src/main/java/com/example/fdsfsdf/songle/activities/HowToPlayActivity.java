package com.example.fdsfsdf.songle.activities;

import android.content.Intent;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.example.fdsfsdf.songle.R;
import com.example.fdsfsdf.songle.manager.AlertDialogManager;
import com.example.fdsfsdf.songle.manager.GameCreator;

public class HowToPlayActivity extends AppCompatActivity {

    private ImageButton settingsButton;
    private RelativeLayout settingsMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_to_play);

        generalActivityIntents();
    }

    /**
     * Method manages the accessing of the settings button.
     */
    private void generalActivityIntents() {
        settingsButton = findViewById(R.id.settingsButton);
        settingsMenu = findViewById(R.id.settingsMenu);

        settingsButton.setZ(9);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int visibility = settingsMenu.getVisibility();
                if(visibility == View.INVISIBLE) {
                    settingsMenu.setZ(8);
                    settingsMenu.setVisibility(View.VISIBLE);
                    settingsIntentLinks();
                } else if(visibility == View.VISIBLE){
                    settingsMenu.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    /**
     * Method manages the buttons (change difficulty, how to play, view completed) which
     * governs which activity is accessed (button name respective).
     *
     * It displays an alert if the user wishes to change their difficulty as this may
     * effect their score via the Difficulty Bonus. If the game hasn't started it asks the user
     * whether they wish to begin
     *
     * Called by generalActivityIntents()
     */
    private void settingsIntentLinks() {

        Button changeDifficultyButton;
        Button howToPlayButton;
        Button viewCompletedSongsButton;

        changeDifficultyButton = findViewById(R.id.sChangeDifficultyButton);
        howToPlayButton = findViewById(R.id.sHowToPlayButton);
        viewCompletedSongsButton = findViewById(R.id.sViewCompletedSongsButton);

        changeDifficultyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GameCreator accessGame = (GameCreator) getApplicationContext();
                boolean gameHasStarted = accessGame.getCurrentSong() != null;
                boolean pastFirstDifficultySelect = accessGame.getDifficultyLevelSystem()
                        .getCurrentDifficulty() != 0;
                boolean accessByCompletedSongsPostGuessOverlay = getIntent()
                        .getBooleanExtra(CompletedSongsActivity.EXTRA_MESSAGE, false);

                if (accessByCompletedSongsPostGuessOverlay) {
                    AlertDialogManager alert = new AlertDialogManager();
                    alert.showAlertDialog(HowToPlayActivity.this, null,
                            getString(R.string.sAlertChangeDifficultyMessage_GuessOverlay),
                            getString(R.string.sAlertChangeDifficultyButton),
                            true, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    settingsMenu.setVisibility(View.INVISIBLE);
                                    startActivity(new Intent(HowToPlayActivity.this, DifficultyActivity.class));
                                }
                            });

                } else if (gameHasStarted && pastFirstDifficultySelect) {
                    AlertDialogManager alert = new AlertDialogManager();
                    alert.showAlertDialog(HowToPlayActivity.this, null,
                            getString(R.string.sAlertChangeDifficultyMessage),
                            getString(R.string.sAlertChangeDifficultyButton),
                            true, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    settingsMenu.setVisibility(View.INVISIBLE);
                                    startActivity(new Intent(HowToPlayActivity.this, DifficultyActivity.class));
                                }
                            });
                } else if (!gameHasStarted) {
                    AlertDialogManager alert = new AlertDialogManager();
                    alert.showAlertDialog(HowToPlayActivity.this, null,
                            getString(R.string.sAlertChangeDifficultyMessage_GameNotStarted),
                            getString(R.string.sAlertChangeDifficultyButton_GameNotStarted),
                            true, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    settingsMenu.setVisibility(View.INVISIBLE);
                                    startActivity(new Intent(HowToPlayActivity.this, DifficultyActivity.class));
                                }
                            });
                } else {
                    settingsMenu.setVisibility(View.INVISIBLE);
                    startActivity(new Intent(HowToPlayActivity.this, DifficultyActivity.class));
                }
            }
        });

        howToPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsMenu.setVisibility(View.INVISIBLE);
            }
        });

        viewCompletedSongsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsMenu.setVisibility(View.INVISIBLE);
                startActivity(new Intent(HowToPlayActivity.this, CompletedSongsActivity.class));
            }
        });

    }

    /**
     * Overrides method to close the settings menu if anywhere outside the settings menu is touched
     * after it is opened.
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        //Used to hide settings menu when screen is touched anywhere but menu
        Rect viewRect = new Rect();
        settingsMenu.getGlobalVisibleRect(viewRect);
        boolean isClickInSettingsMenu = !viewRect.contains((int) ev.getRawX(), (int) ev.getRawY());
        if (isClickInSettingsMenu && settingsMenu.getVisibility() == View.VISIBLE) {
            settingsMenu.setVisibility(View.INVISIBLE);
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }
}