package com.example.fdsfsdf.songle.manager;

import com.example.fdsfsdf.songle.R;

public class DifficultyLevelSystem {

    private int currentDifficulty;

    private final int VERY_EASY = 1;
    private final int EASY = 2;
    private final int NORMAL = 3;
    private final int HARD = 4;
    private final int VERY_HARD = 5;

    private int changeInDifficulty;
    private int initiallySetDifficulty;


    DifficultyLevelSystem() {
        currentDifficulty = 0;
        changeInDifficulty = 0;
    }

    public int getCurrentDifficulty() {
        return currentDifficulty;
    }

    /**
     * Method sets the difficulty and it if it not an initial settings. It updates the
     * changedInDifficulty if the difficulty has been changed.
     *
     * This is because if it is lowered or raised to feed to Points System and prevent cheating.
     * Also primarily used to set initial or future difficulty
     *
     * @param setCurrentDifficulty the difficulty the user is setting based on the button clicked in
     *                             the DifficultyActivity
     */
    public void setCurrentDifficulty(int setCurrentDifficulty) {

        if (currentDifficulty != 0) { //if the difficulty has previously been set so this is an intentional skip

            final int DIFFICULTY_NOT_CHANGED = 0;
            final int DIFFICULTY_DECREASED = 1;
            final int DIFFICULTY_INCREASED = 2;

            if (setCurrentDifficulty == initiallySetDifficulty) {
                //difficulty not changed
                changeInDifficulty = DIFFICULTY_NOT_CHANGED;
            } else if (setCurrentDifficulty < initiallySetDifficulty) {
                //difficulty has been lowered
                changeInDifficulty = DIFFICULTY_DECREASED;

            } else if (setCurrentDifficulty > initiallySetDifficulty) {
                //difficulty has been increased
                changeInDifficulty = DIFFICULTY_INCREASED;
            }
        } else {
            this.initiallySetDifficulty = setCurrentDifficulty; //exists so it they can't decrease then increase and game the system that way
        }

        this.currentDifficulty = setCurrentDifficulty;
    }

    public int getChangeInDifficulty() {
        return changeInDifficulty;
    }

    public int getInitiallySetDifficulty() {
        return initiallySetDifficulty;
    }

    /**
     * Method establishes which map is to be downloaded and used by the player.
     *
     * @return updates the map number held in Game Creator.
     */
    public int whichMapToLoad() {

        final int MAP_1 = 1;
        final int MAP_2 = 2;
        final int MAP_3 = 3;
        final int MAP_4 = 4;
        final int MAP_5 = 5;

        switch (currentDifficulty) {
            case VERY_EASY:
                return MAP_5;
            case EASY:
                return MAP_4;
            case NORMAL:
                return MAP_3;
            case HARD:
                return MAP_2;
            case VERY_HARD:
                return MAP_1;
            default:
                throw new Error("Difficulty level not in range");
        }
    }

    /**
     * Method establishes the string resource which can to be displayed in the Guess activity
     * overlay.
     *
     * @return the resource value for the current difficulty string
     */
    public int getDifficultyString() {
        int difficultyStringResource;

        switch (currentDifficulty) {
            case VERY_EASY:
                difficultyStringResource = R.string.dVeryEasy;
                break;
            case EASY:
                difficultyStringResource = R.string.dEasy;
                break;
            case NORMAL:
                difficultyStringResource = R.string.dNormal;
                break;
            case HARD:
                difficultyStringResource = R.string.dHard;
                break;
            case VERY_HARD:
                difficultyStringResource = R.string.dVeryHard;
                break;
            default:
                throw new Error("Current difficulty not in range");
        }
        return difficultyStringResource;
    }
}
