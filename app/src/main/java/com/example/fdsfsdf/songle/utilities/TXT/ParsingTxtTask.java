package com.example.fdsfsdf.songle.utilities.TXT;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

class ParsingTxtTask {

    // We don’t use namespaces -- just here to get right of require feature

    List<List<String>> readDocument(InputStream in) {
        List<List<String>> linesAndWords = new ArrayList<List<String>>();
        List<String> lines = readLines(in);

        for (int i = 0; i < lines.size(); i++) {
            ArrayList<String> wordsInLine = new ArrayList<>();

            String currentLine = lines.get(i);
            String[] splitWords = currentLine.split(" ");

            for (String word : splitWords) {
                wordsInLine.add(word);
            }
            wordsInLine.trimToSize();
            linesAndWords.add(wordsInLine);
        }

        return linesAndWords;
    }

    private List<String> readLines(InputStream in) {
        List<String> allLines = new ArrayList<String>();
        try {
            // Read all the text returned by the server
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String str;
            while ((str = reader.readLine()) != null) {
                // str is one line of text; readLine() strips the newline character(s)
                allLines.add(str);
            }
            in.close();
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        }
        return allLines;
    }

}
