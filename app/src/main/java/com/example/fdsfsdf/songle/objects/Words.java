package com.example.fdsfsdf.songle.objects;

import android.support.annotation.VisibleForTesting;

import java.util.ArrayList;
import java.util.List;

public class Words {

    private final List<List<String>> words;
    private List<String[]> accessedWords;

    /**
     * Method initialises the class with the songs words and updates them based on the access
     * <p>
     * It also initialises an accessed words array to have a fixed size, the same as the words list
     *
     * @param words parsed list of words downloaded in the difficulty activity
     */
    public Words(List<List<String>> words) {
        this.words = words;

        //Creates a list with the correct sizes for accessing
        List<String[]> accessedWordsSize = new ArrayList<>();
        for (int i = 0; i < words.size(); i++) {
            String[] wordLineSizes = new String[words.get(i).size()];
            accessedWordsSize.add(wordLineSizes);
        }
        this.accessedWords = accessedWordsSize;
    }

    /**
     * Method is used to retrieve the word the user has collected. If it is called by the Map
     * activity it adds the collected word to accessedWords array for use by the lyrics activity
     *
     * @param markerName  the line and word numbers to access the word
     * @param isMapAccess to check if the method is called by the Map Activity
     * @return the word for the map activity
     */
    public String getWord(String markerName, boolean isMapAccess) {
        String word;

        String[] getLineAndWordNumber = markerName.split(":");
        int lineNum = Integer.parseInt(getLineAndWordNumber[0]) - 1; //Line and word numbers have to
        int wordNum = Integer.parseInt(getLineAndWordNumber[1]) - 1; //be offset to work with arrays

        word = words.get(lineNum).get(wordNum);

        if (isMapAccess) {
            accessedWords.get(lineNum)[wordNum] = word;
        }
        return word;
    }


    /**
     * Uses the accessed words to format the lyrics to be displayed by the Lyrics Activity. If a
     * word has not been collected before it replaces this word with '???'.
     *
     * It also makes sure to create a new line if the parsed word is a empty line or the word is at
     * the end of the line.
     *
     * @return a list of the lyrics lines
     */
    public String[] printFormattedLyrics() {
        String[] lyricsLines = new String[accessedWords.size()];


        for (int i = 0; i < lyricsLines.length; i++) {
            String lineWords = "";
            for (int j = 0; j < accessedWords.get(i).length; j++) {

                String accessedWord = accessedWords.get(i)[j];
                if ((accessedWords.get(i).length == 1) && j == 0) { //Case for parsed empty lines

                } else if (j == (accessedWords.get(i).length - 1)) { //Case for last line
                    if (accessedWord == null) {
                        lineWords += ("????");
                    } else {
                        lineWords += (accessedWord);
                    }
                } else {                                            //Case for all other words
                    if (accessedWord == null) {
                        lineWords += ("????" + " ");
                    } else {
                        lineWords += (accessedWord + " ");
                    }
                }
                lyricsLines[i] = lineWords;
            }
        }
        return lyricsLines;
    }

    /**
     * Method is used to reset the accessed words to null in the case that the
     * game is being reset.
     */
    void resetAccessedWords() {

        //Creates a list with the correct sizes for accessing
        List<String[]> accessedWordsSize = new ArrayList<>();
        for (int i = 0; i < words.size(); i++) {
            String[] wordLineSizes = new String[words.get(i).size()];
            accessedWordsSize.add(wordLineSizes);
        }
        this.accessedWords = accessedWordsSize;
    }

    @VisibleForTesting
    public List<List<String>> getWordsList() {
        return words;
    }

    @VisibleForTesting
    public List<String[]> getAccessedWords() {
        return accessedWords;
    }

    @VisibleForTesting
    public void setAccessedWords(List<String[]> accessedWords) {
        this.accessedWords = accessedWords;
    }

}
