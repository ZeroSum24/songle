package com.example.fdsfsdf.songle.utilities.Network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import static android.content.Intent.ACTION_SEND;

public class NetworkReceiver extends BroadcastReceiver {

    public static final String INTERNET_CONNECTED = "com.example.fdsfsdf.songle.INTERNET_CONNECTED";

    private String networkPref; //These are preference values which have to be determined by the
    private String WIFI;        //user. They will be dealt with later on and have been filled in here
    private String ANY;         //for error prevention.

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if(intent.getAction().equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
            networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            if(networkInfo.isConnected()) {
                // Wifi is connected
                Log.d("Inetify", "Wifi is connected: " + String.valueOf(networkInfo));
            }
        } else if (networkPref.equals(WIFI) && networkInfo != null
                && networkInfo.getType() ==
                ConnectivityManager.TYPE_WIFI) {
            // Wi´Fi is connected, so use Wi´Fi
        } else if (networkPref.equals(ANY) && networkInfo != null) {
            // Have a network connection and permission, so use data
        } else {
            // No Wi´Fi and no permission, or no network connection
        }
    }

    public boolean isConnectionDetected(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connMgr != null) {
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null) return true;
        }
        return false;
    }

}
