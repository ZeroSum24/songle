package com.example.fdsfsdf.songle.activities;

import android.content.Intent;
import android.graphics.Rect;
import android.icu.text.IDNA;
import android.support.annotation.VisibleForTesting;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.fdsfsdf.songle.R;
import com.example.fdsfsdf.songle.manager.AlertDialogManager;
import com.example.fdsfsdf.songle.manager.GameCreator;
import com.example.fdsfsdf.songle.utilities.Network.BuildDownloadURL;
import com.example.fdsfsdf.songle.utilities.XML.DownloadXmlTask;
import com.example.fdsfsdf.songle.utilities.Network.NetworkReceiver;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.example.games.basegameutils.BaseGameUtils;

import java.net.URL;
import java.util.concurrent.ExecutionException;

public class MainMenuActivity extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener {

    private Button playButton;
    private Button viewLeaderboardButton;
    private Button viewCompletedButton;
    private GameCreator accessGame;

    private RelativeLayout settingsMenu;


    private static final String TAG = "Main Menu";

    // Request code used to invoke sign in user interactions.
    private static final int RC_SIGN_IN = 9001;
    private static final int RC_LEADERBOARD_UI = 9004;


    // Client used to interact with Google APIs.
    private GoogleApiClient mGoogleApiClient;

    // Are we currently resolving a connection failure?
    private boolean mResolvingConnectionFailure = false;

    // Has the user clicked the sign-in button?
    private boolean mSignInClicked = false;

    // Set to true to automatically start the sign in flow when the Activity starts.
    // Set to false to require the user to click the button in order to sign in.
    private boolean mAutoStartSignInFlow = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        accessGame = ((GameCreator) this.getApplication());
        generalActivityIntents();
        downloadSongList();

        buildGoogleApiClient();

        // set this class to listen for the button clicks
        findViewById(R.id.button_sign_in).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_sign_in:
                // start the sign-in flow
                Log.d(TAG, "Sign-in button clicked");
                mSignInClicked = true;
                mGoogleApiClient.connect();
                break;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected() called. Sign in successful!");

        submitScoreToLeaderboard();
        if (findViewById(R.id.sign_in_bar).getVisibility() == View.VISIBLE) {
            hideSignInBar();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended() called. Trying to reconnect.");
        buildGoogleApiClient();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed() called, result: " + connectionResult);

        if (mResolvingConnectionFailure) {
            Log.d(TAG, "onConnectionFailed() ignoring connection failure; already resolving.");
            return;
        }

        if (mSignInClicked || mAutoStartSignInFlow) {
            mAutoStartSignInFlow = false;
            mSignInClicked = false;
            mResolvingConnectionFailure = BaseGameUtils.resolveConnectionFailure(this, mGoogleApiClient,
                    connectionResult, RC_SIGN_IN, getString(R.string.signin_other_error));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        if (requestCode == RC_SIGN_IN) {
            Log.d(TAG, "onActivityResult with requestCode == RC_SIGN_IN, responseCode="
                    + responseCode + ", intent=" + intent);
            mSignInClicked = false;
            mResolvingConnectionFailure = false;
            if (responseCode == RESULT_OK) {
                buildGoogleApiClient();
            } else {
                BaseGameUtils.showActivityResultError(this, requestCode, responseCode, R.string.signin_other_error);
                showSignInBar();
            }
        }
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart()");
        super.onStart();
        buildGoogleApiClient();

        submitScoreToLeaderboard();
        accessGame.resetGameCreatorPostQuit();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop()");
        super.onStop();
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //helps to set the right color on return from visit
        viewLeaderboardButton.setTextColor(getColor(R.color.colorPrimary_text));
        viewCompletedButton.setTextColor(getColor(R.color.colorPrimary_text));
    }

    /**
     * Method builds the Google Api Client for use by in signing in to Google Play Services
     */
    private synchronized void buildGoogleApiClient() {
        if (isInternetEnabled()) {
            if (mGoogleApiClient == null) {
                mGoogleApiClient = new GoogleApiClient.Builder(this)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .addApi(Games.API).addScope(Games.SCOPE_GAMES)
                        .build();
            }
            mGoogleApiClient.connect();
        } else {
            showProgressBar();
            checkInternetConnection();
        }
    }

    /**
     * Method downloads songs.xml if this has not previously been downloaded. It also
     * checks if internet access is available to do this and if it is not, it displays a Progress
     * Bar whilst the internet connection is established
     *
     * @throws NullPointerException on uncompleted song list download
     * @throws InterruptedException if the download is interrupted
     * @throws ExecutionException   is thrown if the song list is called after the download is interrupted
     *
     * Sets songs.xml as the Song List variable in Game Creator
     */
    private void downloadSongList() {

        boolean isSongListEmpty = accessGame.getSongList().isEmpty();
        if (isInternetEnabled()) {
            if (isSongListEmpty) {
                URL[] urls = new URL[1];
                URL songListDownloadUrl = BuildDownloadURL.buildUrl("songs.txt", null, null);
                urls[0] = songListDownloadUrl;
                try {
                    DownloadXmlTask downloadXmlSongs = new DownloadXmlTask();
                    accessGame.setSongList(downloadXmlSongs.execute(urls).get());
                } catch (NullPointerException e) {
                    System.out.print("Download did not complete");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        } else {
            showProgressBar();
            checkInternetConnection();
        }
    }

    /**
     * Method displays the progress bar.
     * <p>
     * Is called either if the internet is not connected or after the Play button is clicked.
     */
    private void showProgressBar() {
        ProgressBar progressBar = findViewById(R.id.pb_loading_indicator);
        progressBar.setVisibility(View.VISIBLE);
        playButton.setVisibility(View.INVISIBLE);
        viewLeaderboardButton.setVisibility(View.INVISIBLE);
        viewCompletedButton.setVisibility(View.INVISIBLE);
    }

    /**
     * Shows the "sign in" bar (explanation and button).
     */
    private void showSignInBar() {
        Log.d(TAG, "Showing sign in bar");
        findViewById(R.id.sign_in_bar).setVisibility(View.VISIBLE);
    }

    /**
     * Hides the "sign in" bar (explanation and button).
     */
    private void hideSignInBar() {
        Log.d(TAG, "Showing sign in bar");
        findViewById(R.id.sign_in_bar).setVisibility(View.GONE);
    }

    /**
     * Method manages the three primary buttons (play, view leaderboard and view completed) which governs
     * which activities (difficulty, leaderboard and completed songs respectively) are to be
     * accessed through intents
     * <p>
     * It also highlights the chosen button to show the user which has been clicked.
     */
    private void generalActivityIntents() {

        ImageButton settingsButton = findViewById(R.id.settingsButton);

        playButton = findViewById(R.id.wPlayButton);
        viewLeaderboardButton = findViewById(R.id.wLeaderboard);
        viewCompletedButton = findViewById(R.id.wTotalCompletedSongsButton);
        settingsMenu = findViewById(R.id.settingsMenu);


        //functionality for the buttons

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playButton.setTextColor(getColor(R.color.colorIcons));
                showProgressBar();
                startActivity(new Intent(MainMenuActivity.this, DifficultyActivity.class));
            }
        });

        viewLeaderboardButton.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                viewLeaderboardButton.setTextColor(getColor(R.color.colorIcons));
                showLeaderboard();
            }
        });

        viewCompletedButton.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainMenuActivity.this, CompletedSongsActivity.class));
                viewCompletedButton.setTextColor(getColor(R.color.colorIcons));
            }
        });

        settingsButton.setZ(9); //Sets the settingsButton and settingsMenu higher than all else
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int visibility = settingsMenu.getVisibility();
                if (visibility == View.INVISIBLE) {
                    settingsMenu.setZ(8);
                    settingsMenu.setVisibility(View.VISIBLE);
                    settingsIntentLinks();
                } else if (visibility == View.VISIBLE) {
                    settingsMenu.setVisibility(View.INVISIBLE);
                }
            }
        });

    }

    /**
     * Method displays a SnackBar to allow the user to recheck the internet connection. Since it is
     * called if the internet is not established, when download songs.xml or connecting to
     * google_play, it starts these when the internet is established.
     * The SnackBar hides the ProgressBar if the internet connection has been established.
     * <p>
     * The method is repeatedly called if the internet is not established, the progress bar will not
     * be dismissed until it is. The swipe-to-dismiss functionality has been disabled to prevent
     * the user being stuck on a permanent ProgressBar
     */
    private void checkInternetConnection() {

        //Standard code for SnackBar updated to relate to internet Connection
        Snackbar internetSnackBar = Snackbar.make(findViewById(R.id.wCoordinatorLayout),
                R.string.noInternet, Snackbar.LENGTH_INDEFINITE);
        final View snackBarView = internetSnackBar.getView();
        internetSnackBar.setAction(R.string.retry, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressBar progressBar = findViewById(R.id.pb_loading_indicator);
                progressBar.setVisibility(View.INVISIBLE);
                playButton.setVisibility(View.VISIBLE);
                viewLeaderboardButton.setVisibility(View.VISIBLE);
                viewCompletedButton.setVisibility(View.VISIBLE);
                downloadSongList();
                buildGoogleApiClient();
            }
        });
        internetSnackBar.show();

        //Code to stop swipe ability of SnackBar and avoid functionality
        //Code sourced from https://stackoverflow.com/questions/34031476/how-to-disable-snackbars-swipe-to-dismiss-behavior
        snackBarView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                snackBarView.getViewTreeObserver().removeOnPreDrawListener(this);
                ((CoordinatorLayout.LayoutParams) snackBarView.getLayoutParams()).setBehavior(null);
                return true;
            }
        });
    }

    /**
     * Method checks whether the internet connection has been established by accessing the Network
     * Receiver
     *
     * @return Nothing. Sets songs.xml as the Song List variable in Game Creator
     */
    private boolean isInternetEnabled() {
        NetworkReceiver receiver = new NetworkReceiver();
        return receiver.isConnectionDetected(getApplicationContext());
    } //Called in connectionCheckers()

    /**
     * Method manages the buttons (change difficulty, how to play, view completed) which
     * governs which activity is accessed (button name respective).
     * <p>
     * Called by generalActivityIntents()
     */
    private void settingsIntentLinks() {

        Button changeDifficultyButton;
        Button howToPlayButton;
        Button viewCompletedSongsButton;

        changeDifficultyButton = findViewById(R.id.sChangeDifficultyButton);
        howToPlayButton = findViewById(R.id.sHowToPlayButton);
        viewCompletedSongsButton = findViewById(R.id.sViewCompletedSongsButton);

        changeDifficultyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsMenu.setVisibility(View.INVISIBLE);
            }
        });

        howToPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsMenu.setVisibility(View.INVISIBLE);
                startActivity(new Intent(MainMenuActivity.this, HowToPlayActivity.class));
            }
        });

        viewCompletedSongsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsMenu.setVisibility(View.INVISIBLE);
                startActivity(new Intent(MainMenuActivity.this, CompletedSongsActivity.class));
            }
        });


    }

    /**
     * The Leaderboard Activity is started if the user has been signed in and otherwise it displays
     * the sign-in bar to allow the user to re-try their connection
     * <p>
     * Called by generalActivityIntents()
     */
    private void showLeaderboard() {

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            String LEADERBOARD_ID = getString(R.string.google_leaderboard_key);

            startActivityForResult(
                    Games.Leaderboards.getLeaderboardIntent(mGoogleApiClient, LEADERBOARD_ID),
                    RC_LEADERBOARD_UI);
        } else {
            showSignInBar();
            viewLeaderboardButton.setTextColor(getColor(R.color.colorPrimary_text));
        }
    }

    /**
     * Method submits the player's score to the leaderboard if one exists. That is if the score has
     * been set to a value other than -1 which it has been initialised to.
     * <p>
     * Only is called when the user has been signed-in on connect or on the start of the activity if
     * the user has previously been able to sign-in.
     * <p>
     * It resets the score if the user has successfully submitted theirs.
     */
    private void submitScoreToLeaderboard() {

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) { //Player is signed-in

            int scoreToSubmit = accessGame.getUsersScoreToSubmit();

            if (scoreToSubmit > 0) {
                //Submit the score
                if (mGoogleApiClient.isConnected()) {
                    String LEADERBOARD_ID = getString(R.string.google_leaderboard_key);
                    Games.Leaderboards.submitScore(mGoogleApiClient, LEADERBOARD_ID, scoreToSubmit);
                }
            }
        }
    }

    /**
     * Overrides method to allow prevent the user accessing the last activity and allowing them the
     * option to quit the game.
     */
    @Override
    public void onBackPressed() {
        AlertDialogManager alert = new AlertDialogManager();
        alert.showAlertDialog(MainMenuActivity.this, null,
                getString(R.string.cQuitAlertMMHeader), getString(R.string.cQuitAlertButton),
                true, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                        System.exit(0);
                    }
                });
    }

    /**
     * Overrides method to close the settings menu if anywhere outside the settings menu is touched
     * after it is opened.
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        //Used to hide settings menu when screen is touched anywhere but menu
        Rect viewRect = new Rect();
        settingsMenu.getGlobalVisibleRect(viewRect);
        boolean isClickInSettingsMenu = !viewRect.contains((int) ev.getRawX(), (int) ev.getRawY());
        if (isClickInSettingsMenu && settingsMenu.getVisibility() == View.VISIBLE) {
            settingsMenu.setVisibility(View.INVISIBLE);
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }

}
