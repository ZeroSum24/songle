package com.example.fdsfsdf.songle.objects;


import com.example.fdsfsdf.songle.R;


public class Placemark {

    private final String name;
    private final String description;
    private final String styleUrl;
    private final Point point;
    private boolean placemarkRemoved = false;

    public Placemark(String name, String description, String styleUrl, Point point) {
        this.name = name;
        this.description = description;
        this.styleUrl = styleUrl;
        this.point = point;
    }

    public String getName() {return name;}

    /**
     * Method updates the markers description to be more exciting words for the user.
     *
     * @return an integer which the activity can use to access the relevant string resource
     */
    public int getDescription() {

        int fancyDescription;
        switch (description) {
            case "unclassified": fancyDescription = R.string.iUnclassified; break;
            case "boring": fancyDescription = R.string.iBoring; break;
            case "notboring": fancyDescription = R.string.iNotBoring; break;
            case "interesting": fancyDescription = R.string.iInteresting; break;
            case "veryinteresting": fancyDescription = R.string.iVeryInteresting; break;
            default: throw new Error("Description does not exist");
        }
        return fancyDescription;
    }

    public String getStyleUrl() {return styleUrl;}

    public Point getPoint() {
        return point;
    }

    /**
     * Method returns the marker image to be displayed on the map, depending on the interest level
     * of the word
     *
     * @return an integer which the activity can use to access the relevant map_icon resource
     */
    public int returnMarkerImage() {

        int icon;
        switch (styleUrl) {
            case "#unclassified": icon = R.drawable.map_icon_unclassified; break;
            case "#boring": icon = R.drawable.map_icon_boring; break;
            case "#notboring": icon = R.drawable.map_icon_notboring; break;
            case "#interesting": icon = R.drawable.map_icon_interesting; break;
            case "#veryinteresting": icon = R.drawable.map_icon_veryinteresting; break;
            default: throw new Error("StyleUrl does not exist");
        }
        return icon;
    }

    public boolean isPlacemarkRemoved() {return placemarkRemoved;}

    public void setPlacemarkRemoved(boolean isRemoved) {
        this.placemarkRemoved = isRemoved;
    }


}
