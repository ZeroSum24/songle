package com.example.fdsfsdf.songle.activities;

import android.content.Intent;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fdsfsdf.songle.R;
import com.example.fdsfsdf.songle.manager.AlertDialogManager;
import com.example.fdsfsdf.songle.manager.GameCreator;

public class LyricsActivity extends AppCompatActivity {

    private Button mapButton;
    private Button guessButton;
    private RelativeLayout settingsMenu;

    private GameCreator accessGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lyrics);

        generalActivityIntents();
        displayLyrics();
        displayPointsSystem();

    }

    @Override
    protected void onStart() {
        super.onStart();
        displayPointsSystem();
    }

    /**
     * Method accesses and displays the lyrics on the activity
     */
    private void displayLyrics() {

        accessGame = (GameCreator) getApplicationContext();
        String[] lyricsLines = accessGame.getCurrentSong().getWords().printFormattedLyrics();

        RecyclerView listLyrics = findViewById(R.id.lListLyrics);

        LyricsAdapter mAdapter = new LyricsAdapter(lyricsLines);
        mAdapter.setHasStableIds(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        listLyrics.setLayoutManager(mLayoutManager);
        listLyrics.setItemAnimator(new DefaultItemAnimator());
        listLyrics.setAdapter(mAdapter);

        listLyrics.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), listLyrics,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        int pos = position + 1;
                        Toast.makeText(LyricsActivity.this,
                                "Line " + Integer.toString(pos), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onLongClick(View view, int position) {
                    }

                }));

    }

    /**
     * Method checks the points which the user has collected and displays them on the activity.
     */
    private void displayPointsSystem() {
        //POINTS SYSTEM

        //Initialises the Views here
        TextView setCurrentPoints = findViewById(R.id.currentPoints);
        TextView setTotalPoints = findViewById(R.id.totalPoints);

        //Accesses the data needed to display the views
        String currentPoints = Integer.toString(accessGame.getPointsSystem().getCurrentPoints());
        String totalPoints = Integer.toString(accessGame.getPointsSystem().getTotalPoints());

        //Sets the views here
        setCurrentPoints.setText(currentPoints);
        setTotalPoints.setText(totalPoints);

    }

    /**
     * Method manages the two intent buttons (map and guess) which
     * governs which activity is accessed (button name respective). It also governs the settings
     * button.
     * <p>
     * It also highlights the chosen button to show the user which has been clicked.
     */
    private void generalActivityIntents() {

        ImageButton settingsButton;

        //GENERAL ACTIVITY FUNCTIONS

        mapButton = findViewById(R.id.leftButton);
        guessButton = findViewById(R.id.rightButton);
        settingsButton = findViewById(R.id.settingsButton);
        settingsMenu = findViewById(R.id.settingsMenu);

        mapButton.setText(R.string.cMap);

        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapButton.setTextColor(getColor(R.color.colorIcons));
                startActivity(new Intent(LyricsActivity.this, MapsActivity.class));
            }
        });

        guessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guessButton.setTextColor(getColor(R.color.colorIcons));
                startActivity(new Intent(LyricsActivity.this, GuessActivity.class));
            }
        });
        settingsButton.setZ(9);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int visibility = settingsMenu.getVisibility();
                if (visibility == View.INVISIBLE) {
                    settingsMenu.setZ(8);
                    settingsMenu.setVisibility(View.VISIBLE);
                    settingsIntentLinks();
                } else if (visibility == View.VISIBLE) {
                    settingsMenu.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    /**
     * Method manages the buttons (change difficulty, how to play, view completed) which
     * governs which activity is accessed (button name respective).
     * <p>
     * It displays an alert if the user wishes to change their difficulty as this may
     * effect their score via the Difficulty Bonus.
     * <p>
     * Called by generalActivityIntents()
     */
    private void settingsIntentLinks() {
        Button changeDifficultyButton;
        Button howToPlayButton;
        Button viewCompletedSongsButton;

        changeDifficultyButton = findViewById(R.id.sChangeDifficultyButton);
        howToPlayButton = findViewById(R.id.sHowToPlayButton);
        viewCompletedSongsButton = findViewById(R.id.sViewCompletedSongsButton);

        changeDifficultyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialogManager alert = new AlertDialogManager();
                alert.showAlertDialog(LyricsActivity.this, null,
                        getString(R.string.sAlertChangeDifficultyMessage),
                        getString(R.string.sAlertChangeDifficultyButton),
                        true, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                settingsMenu.setVisibility(View.INVISIBLE);
                                startActivity(new Intent(LyricsActivity.this, DifficultyActivity.class));
                            }
                        });
            }
        });

        howToPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsMenu.setVisibility(View.INVISIBLE);
                startActivity(new Intent(LyricsActivity.this, HowToPlayActivity.class));
            }
        });

        viewCompletedSongsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsMenu.setVisibility(View.INVISIBLE);
                startActivity(new Intent(LyricsActivity.this, CompletedSongsActivity.class));
            }
        });

    } //Called by generalActivityIntents

    /**
     * Overrides method to allow prevent the user accessing the last activity and allowing them the
     * option to quit the session. It also updates the users score to be submitted by the
     * MainMenuActivity.
     */
    @Override
    public void onBackPressed() {
        AlertDialogManager alert = new AlertDialogManager();
        alert.showAlertDialog(LyricsActivity.this, null,
                getString(R.string.cQuitAlertHeader), getString(R.string.cQuitAlertButton),
                true, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int pointsToSubmit = accessGame.getPointsSystem().pointsToSubmit();
                        accessGame.setUsersScoreToSubmit(pointsToSubmit);

                        startActivity(new Intent(LyricsActivity.this, MainMenuActivity.class));
                    }
                });
    }

    /**
     * Overrides method to close the settings menu if anywhere outside the settings menu is touched
     * after it is opened.
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        //Used to hide settings menu when screen is touched anywhere but menu
        Rect viewRect = new Rect();
        settingsMenu.getGlobalVisibleRect(viewRect);
        boolean isClickInSettingsMenu = !viewRect.contains((int) ev.getRawX(), (int) ev.getRawY());
        if (isClickInSettingsMenu && settingsMenu.getVisibility() == View.VISIBLE) {
            settingsMenu.setVisibility(View.INVISIBLE);
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }
}

/**
 * Class is used to create a custom Lyrics adapter for the Recycler View used to display the songs
 * lyrics.
 */
class LyricsAdapter extends RecyclerView.Adapter<LyricsAdapter.MyViewHolder> {

    private String[] lyricsLines;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView number, lyricsLine;

        MyViewHolder(View view) {
            super(view);
            number = (TextView) view.findViewById(R.id.lRLyricsLineNumber);
            lyricsLine = (TextView) view.findViewById(R.id.lRLyricsLine);
        }
    }

    LyricsAdapter(String[] lyricsLines) {
        this.lyricsLines = lyricsLines;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.res_lyrics__line, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        String currentLine = lyricsLines[position];


        String lineNumber;
        if (position >= 0 && position < 9) { //For printing line numbers
            lineNumber = "0" + Integer.toString(position + 1);
        } else {
            lineNumber = Integer.toString(position + 1);
        }

        holder.number.setText(lineNumber);
        holder.lyricsLine.setText(currentLine);

    }

    @Override
    public int getItemCount() {
        return lyricsLines.length;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
