package com.example.fdsfsdf.songle.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fdsfsdf.songle.R;
import com.example.fdsfsdf.songle.manager.AlertDialogManager;
import com.example.fdsfsdf.songle.manager.GameCreator;
import com.example.fdsfsdf.songle.objects.Song;

public class CompletedSongsActivity extends AppCompatActivity {

    private RelativeLayout settingsMenu;
    private Song[] completedSongList;

    public static final String EXTRA_MESSAGE = "com.example.songle.GUESS_OVERLAY_ACCESS";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completed_songs);

        generalActivityIntents();
        displayCompletedSongs();
    }


    /**
     * Method displays the list of songs in a Recycler View, providing the songs details if the song
     * has been completed. Otherwise it fills the details with '???'.
     */
    private void displayCompletedSongs() {
        GameCreator accessGame = (GameCreator) getApplicationContext();

        completedSongList = accessGame.getCompletedSongList();

        RecyclerView cSDetailsContainer = findViewById(R.id.csDetailsContainer);

        CompletedSongsAdapter mAdapter = new CompletedSongsAdapter(completedSongList);
        mAdapter.setHasStableIds(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        cSDetailsContainer.setLayoutManager(mLayoutManager);
        cSDetailsContainer.setItemAnimator(new DefaultItemAnimator());
        cSDetailsContainer.addItemDecoration(
                new MyDividerItemDecoration(this, R.drawable.res_divider,
                        LinearLayoutManager.VERTICAL, 10));
        cSDetailsContainer.setAdapter(mAdapter);

        cSDetailsContainer.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), cSDetailsContainer, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                int pos = position + 1;
                Toast.makeText(CompletedSongsActivity.this,
                        Integer.toString(pos) + " Clicked", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

                Song clickedOnSong = completedSongList[position];

                if (clickedOnSong != null) {
                    Uri uri = Uri.parse(clickedOnSong.getLink());
                    final Intent intent = new Intent(Intent.ACTION_VIEW, uri);

                    String viewSongOnYoutube = getString(R.string.tViewSongYoutubeHeaderPt1)
                            + "  \"" + clickedOnSong.getTitle() + "\" "
                            + getString(R.string.tViewSongYoutubeHeaderPt2);

                    final AlertDialogManager alert = new AlertDialogManager();
                    alert.showAlertDialog(CompletedSongsActivity.this, null,
                            viewSongOnYoutube, getString(R.string.tViewSongYoutubeButton),
                            true, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(intent);
                                    alert.dismissAlertDialog();
                                }
                            });
                }
            }

        }));

    }

    /**
     * Method manages the accessing of the settings button.
     */
    private void generalActivityIntents() {

        ImageButton settingsButton;

        settingsButton = findViewById(R.id.settingsButton);
        settingsMenu = findViewById(R.id.settingsMenu);

        settingsButton.setZ(9);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int visibility = settingsMenu.getVisibility();
                if (visibility == View.INVISIBLE) {
                    settingsMenu.setZ(8);
                    settingsMenu.setVisibility(View.VISIBLE);
                    settingsIntentLinks();
                } else if (visibility == View.VISIBLE) {
                    settingsMenu.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    /**
     * Method manages the buttons (change difficulty, how to play, view completed) which
     * governs which activity is accessed (button name respective).
     * <p>
     * It displays an alert if the user wishes to change their difficulty as this may
     * effect their score via the Difficulty points Bonus. If the game hasn't started it asks the
     * user whether they wish to begin.
     * <p>
     * Called by generalActivityIntents()
     */
    private void settingsIntentLinks() {

        Button changeDifficultyButton;
        Button howToPlayButton;
        Button viewCompletedSongsButton;

        changeDifficultyButton = findViewById(R.id.sChangeDifficultyButton);
        howToPlayButton = findViewById(R.id.sHowToPlayButton);
        viewCompletedSongsButton = findViewById(R.id.sViewCompletedSongsButton);

        changeDifficultyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GameCreator accessGame = (GameCreator) getApplicationContext();
                boolean gameHasStarted = accessGame.getCurrentSong() != null;
                boolean pastFirstDifficultySelect = accessGame.getDifficultyLevelSystem()
                        .getCurrentDifficulty() != 0;
                boolean accessByGuessOverlay = getIntent().getBooleanExtra(GuessActivity.EXTRA_MESSAGE, false);

                if (accessByGuessOverlay) {
                    AlertDialogManager alert = new AlertDialogManager();
                    alert.showAlertDialog(CompletedSongsActivity.this, null,
                            getString(R.string.sAlertChangeDifficultyMessage_GuessOverlay),
                            getString(R.string.sAlertChangeDifficultyButton),
                            true, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    settingsMenu.setVisibility(View.INVISIBLE);
                                    startActivity(new Intent(CompletedSongsActivity.this, DifficultyActivity.class));
                                }
                            });
                } else if (gameHasStarted && pastFirstDifficultySelect) {
                    AlertDialogManager alert = new AlertDialogManager();
                    alert.showAlertDialog(CompletedSongsActivity.this, null,
                            getString(R.string.sAlertChangeDifficultyMessage),
                            getString(R.string.sAlertChangeDifficultyButton),
                            true, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    settingsMenu.setVisibility(View.INVISIBLE);
                                    startActivity(new Intent(CompletedSongsActivity.this, DifficultyActivity.class));
                                }
                            });
                } else if (!gameHasStarted) {
                    AlertDialogManager alert = new AlertDialogManager();
                    alert.showAlertDialog(CompletedSongsActivity.this, null,
                            getString(R.string.sAlertChangeDifficultyMessage_GameNotStarted),
                            getString(R.string.sAlertChangeDifficultyButton_GameNotStarted),
                            true, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    settingsMenu.setVisibility(View.INVISIBLE);
                                    startActivity(new Intent(CompletedSongsActivity.this, DifficultyActivity.class));
                                }
                            });
                } else {
                    settingsMenu.setVisibility(View.INVISIBLE);
                    startActivity(new Intent(CompletedSongsActivity.this, DifficultyActivity.class));
                }
            }
        });

        howToPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsMenu.setVisibility(View.INVISIBLE);
                boolean accessByGuessOverlay = getIntent()
                        .getBooleanExtra(GuessActivity.EXTRA_MESSAGE, false);

                Intent intent = new Intent(CompletedSongsActivity.this, HowToPlayActivity.class);

                if (accessByGuessOverlay) {
                    intent.putExtra(EXTRA_MESSAGE, true);
                }
                startActivity(intent);
            }
        });

        viewCompletedSongsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsMenu.setVisibility(View.INVISIBLE);
            }
        });


    }

    /**
     * Overrides method to close the settings menu if anywhere outside the settings menu is touched
     * after it is opened.
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        Rect viewRect = new Rect();
        settingsMenu.getGlobalVisibleRect(viewRect);
        boolean isClickInSettingsMenu = !viewRect.contains((int) ev.getRawX(), (int) ev.getRawY());
        if (isClickInSettingsMenu && settingsMenu.getVisibility() == View.VISIBLE) {
            settingsMenu.setVisibility(View.INVISIBLE);
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }

}

/**
 * Class is used to create a custom Completed Songs Adapter to display them correctly in the
 * RecyclerView.
 */
class CompletedSongsAdapter extends RecyclerView.Adapter<CompletedSongsAdapter.MyViewHolder> {

    private Song[] completedSongList;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView number, title, artist;

        MyViewHolder(View view) {
            super(view);
            number = (TextView) view.findViewById(R.id.cSNumberOfSong);
            title = (TextView) view.findViewById(R.id.cSTitleOfSong);
            artist = (TextView) view.findViewById(R.id.cSArtistOfSong);
        }
    }


    CompletedSongsAdapter(Song[] completedSongList) {
        this.completedSongList = completedSongList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.res_completed__list_details, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Song current = completedSongList[position];

        String lineNumber;
        if (position >= 0 && position < 9) { //For printing line numbers
            lineNumber = "0" + Integer.toString(position + 1);
        } else {
            lineNumber = Integer.toString(position + 1);
        }

        holder.number.setText(lineNumber);

        if (current != null) {
            holder.number.setText(current.getNumber());
            holder.title.setText(current.getTitle());
            holder.artist.setText(current.getArtist());
        } else {
            holder.number.setText(lineNumber);
        }
    }

    @Override
    public int getItemCount() {
        return completedSongList.length;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

/**
 * Class is used to create a custom Divider for the RecyclerView.
 */
class MyDividerItemDecoration extends RecyclerView.ItemDecoration {

    private static final int[] ATTRS = new int[]{
            android.R.attr.listDivider
    };

    private static final int HORIZONTAL_LIST = LinearLayoutManager.HORIZONTAL;

    private static final int VERTICAL_LIST = LinearLayoutManager.VERTICAL;

    private Drawable mDivider;
    private int mOrientation;
    private int margin;
    private Context context;

    MyDividerItemDecoration(Context context, int resId, int orientation, int margin) {
        this.margin = margin;
        this.context = context;
        mDivider = ContextCompat.getDrawable(context, resId);
        setOrientation(orientation);
    }

    private void setOrientation(int orientation) {
        if (orientation != HORIZONTAL_LIST && orientation != VERTICAL_LIST) {
            throw new IllegalArgumentException("invalid orientation");
        }
        mOrientation = orientation;
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        if (mOrientation == VERTICAL_LIST) {
            drawVertical(c, parent);
        } else {
            drawHorizontal(c, parent);
        }
    }

    private void drawVertical(Canvas c, RecyclerView parent) {
        final int left = parent.getPaddingLeft();
        final int right = parent.getWidth() - parent.getPaddingRight();

        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();
            final int top = child.getBottom() + params.bottomMargin;
            final int bottom = top + mDivider.getIntrinsicHeight();
            mDivider.setBounds(left + dpToPx(margin), top, right - dpToPx(margin), bottom);
            mDivider.draw(c);
        }
    }

    private void drawHorizontal(Canvas c, RecyclerView parent) {
        final int top = parent.getPaddingTop();
        final int bottom = parent.getHeight() - parent.getPaddingBottom();

        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();
            final int left = child.getRight() + params.rightMargin;
            final int right = left + mDivider.getIntrinsicHeight();
            mDivider.setBounds(left, top + dpToPx(margin), right, bottom - dpToPx(margin));
            mDivider.draw(c);
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if (mOrientation == VERTICAL_LIST) {
            outRect.set(0, 0, 0, mDivider.getIntrinsicHeight());
        } else {
            outRect.set(0, 0, mDivider.getIntrinsicWidth(), 0);
        }
    }

    private int dpToPx(int dp) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}

/**
 * Class is used to implement custom touch events for each individual list item in the RecyclerView.
 * These events allow the user to view access a Youtube link if they long click on the collected
 * list item.
 *
 * This class is also used in the Lyrics activity to allow the user to click each individual line
 * to be informed about which has been selected.
 *
 */
class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

    interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    private GestureDetector gestureDetector;
    private ClickListener clickListener;

    RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
        this.clickListener = clickListener;
        gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                if (child != null && clickListener != null) {
                    clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                }
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

        View child = rv.findChildViewUnder(e.getX(), e.getY());
        if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
            clickListener.onClick(child, rv.getChildPosition(child));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}
