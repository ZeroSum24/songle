package com.example.fdsfsdf.songle.utilities.KML;

import android.os.AsyncTask;
import android.util.Log;

import com.example.fdsfsdf.songle.objects.Map;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadKmlTask extends AsyncTask<URL, Void, Map> {

    @Override
    protected Map doInBackground(URL... urls) {
        try {
            return loadKmlFromNetwork(urls[0]);
        } catch (IOException e) {
            throw new Error("Unable to load content. Check your network connection");
        } catch (XmlPullParserException e) {
            throw new Error("Error parsing KML");
        }
    }

    @Override
    protected void onPostExecute(Map result) {
        // Do something with result
        Log.e("DownloadKML", "MapsList " + result);
        Log.e("DownloadKML", "MapsList " + "COMPLETED");
    }

    private Map loadKmlFromNetwork(URL url) throws XmlPullParserException, IOException {
        Map songMap;
        try (InputStream stream = downloadUrl(url)) {
            // Do something with stream e.g. parse as XML, build result
            //added code
            ParsingKmlTask mapParser = new ParsingKmlTask();
            songMap = mapParser.parse(stream);
        }
        return songMap;
    }

    // Given a string representation of a URL, sets up a connection and gets
    // an input stream.
    private InputStream downloadUrl(URL url) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        // Also available: HttpsURLConnection
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // Starts the query
        conn.connect();
        return conn.getInputStream();
    }


}
