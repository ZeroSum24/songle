package com.example.fdsfsdf.songle.manager;

import android.app.Application;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import com.example.fdsfsdf.songle.objects.Map;
import com.example.fdsfsdf.songle.objects.Song;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Class manages all of the persistent game variables throughout a session. It holds the instances
 * for the Points System and the Difficulty Level System here.
 * <p>
 * Alongside this it manages which song is chosen from the list of songs it holds and it keeps the
 * leaderboard score for the user to submit
 */

public class GameCreator extends Application {

    private List<Song> songList = new ArrayList<Song>();
    private Song[] completedSongList;

    private Song currentSong = null;
    private boolean skippedSong = false;
    private boolean newSongNeeded = false;

    private DifficultyLevelSystem difficultyLevelSystem = new DifficultyLevelSystem();
    private PointsSystem pointsSystem = new PointsSystem();

    private int usersScoreToSubmit;


    public DifficultyLevelSystem getDifficultyLevelSystem() {
        return difficultyLevelSystem;
    }

    public PointsSystem getPointsSystem() {
        return pointsSystem;
    }

    public List<Song> getSongList() {
        return songList;
    }

    /**
     * Method sets the song list after downloading and parsing songs.xml
     *
     * @param songList parsed songs from songs.xml
     */
    public void setSongList(List<Song> songList) {
        this.songList = songList;
        this.completedSongList = new Song[songList.size()];
        Log.e("SetSongList", "DownloadedSongList empty: " + (this.songList.isEmpty()));
        Log.e("SetSongList", "DownloadedSongList size: " + (this.songList.size()));
    }

    /**
     * Method chooses the song by generating a random number to be used to accesses songList and set
     * the current song.
     * <p>
     * The current song is selected from songs which have not been completed
     */
    public void chooseSong() {

        Random number = new Random();

        int exclusiveLimit = songList.size(); //exclusive limit won't create array bounds as exclusive, 0 is inclusive btw
        Log.e("ChooseSong", "SongList size: " + (exclusiveLimit));
        int rGenNum;

        do {
            rGenNum = number.nextInt(exclusiveLimit);// should repeat the random guessing again if same
            // position in list
            currentSong = songList.get(rGenNum);
        } while (Arrays.asList(completedSongList).contains(currentSong));
        newSongNeeded = false;
        Log.e("ChooseSong", "Random Number: " + (rGenNum));
        Log.e("ChooseSong", "Current Song: " + currentSong.getTitle());

    }

    /**
     * Method chooses the song by generating a random number to be used to accesses songList and set
     * the current song.
     * <p>
     * The current song is selected from songs which have not been completed. It does not include
     * the current song which has been skipped.
     */
    public void chooseAndSkipPreviousSong() {

        int skippedPos = songList.indexOf(currentSong);
        currentSong.resetSong();    //Resets song so it can be played afresh later in the game

        Random number = new Random();

        int exclusiveLimit = songList.size();
        int rGenNum;

        do {
            rGenNum = number.nextInt(exclusiveLimit);// should repeat the random guessing again if same // position in list
            currentSong = songList.get(rGenNum);
        } while (rGenNum == skippedPos || Arrays.asList(completedSongList).contains(currentSong));
        Log.e("ChooseSongAfterSkipped", "Random Number: " + (rGenNum));
        skippedSong = false;
        newSongNeeded = false;
    }

    public Song getCurrentSong() {
        return currentSong;
    }

    public Boolean getSkippedSong() {
        return skippedSong;
    }

    public void setSkippedSong(Boolean skippedSong) {
        this.skippedSong = skippedSong;
    }

    public Boolean getNewSongNeeded() {
        return newSongNeeded;
    }

    public void setNewSongNeeded(Boolean newSongNeeded) {
        this.newSongNeeded = newSongNeeded;
    }

    /**
     * Method adds the current song to the list of completed songs
     */
    public void addSongToCompletedSongList() {
        int positionInSongList = Integer.parseInt(currentSong.getNumber()) - 1;
        this.completedSongList[positionInSongList] = currentSong;
    }

    public Song[] getCompletedSongList() {
        return completedSongList;
    }

    public int getUsersScoreToSubmit() {
        return usersScoreToSubmit;
    }

    public void setUsersScoreToSubmit(int usersScoreToSubmit) {
        this.usersScoreToSubmit = usersScoreToSubmit;
    }

    /**
     * Method which handles the case that all the songs in the game have been completed.
     * <p>
     * It checks to see whether all the songs have been completed. In which case they will be
     * reset by resetGameIfFinished().
     *
     * @return whether the completed song list is full
     */
    public boolean checkIfCompletedSongListIsFull() {

        boolean isCompletedSongListFull = false;

        for (Song cur : completedSongList) {
            boolean isSongFinished = cur != null;

            if (isSongFinished) {
                isCompletedSongListFull = true;

            } else {
                isCompletedSongListFull = false;
                break;
            }
        }
        return isCompletedSongListFull;
    }

    /**
     * Method which handles the case that all the songs in the game have been completed.
     * <p>
     * It resets the completed songs to null so the user can continue playing afresh. It also
     * resets the variables in Songs which have been changed throughout the course of play.
     */
    public void resetGameIfFinished() {
        this.completedSongList = new Song[songList.size()];

        for (Song finishedSong : songList) {
            finishedSong.resetSong();
        }
    }

    /**
     * Method resets Game Creator variables to their initial state when the game has been quit
     */
    public void resetGameCreatorPostQuit() {
        this.currentSong = null;
        this.skippedSong = false;
        this.newSongNeeded = false;

        this.pointsSystem = new PointsSystem();
        this.difficultyLevelSystem = new DifficultyLevelSystem();
    }

}

