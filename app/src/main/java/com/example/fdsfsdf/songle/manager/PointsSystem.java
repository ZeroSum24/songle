package com.example.fdsfsdf.songle.manager;

import android.support.annotation.VisibleForTesting;

public class PointsSystem {

    private int totalPoints;
    private int totalSongPoints;
    private int currentPoints;


    private final int Difficulty_VERY_EASY = 1;
    private final int Difficulty_EASY = 2;
    private final int Difficulty_NORMAL = 3;
    private final int Difficulty_HARD = 4;
    private final int Difficulty_VERY_HARD = 5;

    PointsSystem() {
        totalPoints = 0;
        totalSongPoints = 0;
        currentPoints = 0;
    }

    public int getCurrentPoints() {
        // Getter method for current points;

        return currentPoints;
    }

    public int getTotalSongPoints() {
        // Getter method for new Total points;

        return totalSongPoints;
    }

    public int getTotalPoints() {
        // Getter method for total points;

        return totalPoints;
    }

    /**
     * Method evaluates the Interest level and updates the current points based on this. Also
     * returns the points it is updating as a String to be displayed for user
     *
     * @param markerInterestLevel Used to check compare interest level and update the points based
     *                            on this. Variable feed into method is styleUrl of Placemark as
     *                            this is a fixed downloaded map constant (which prevents having to
     *                            use updated and translatable marker descriptions)
     */
    public String returnEarnedPoints(String markerInterestLevel) {

        final int boringPoints = 10;
        final int notBoringPoints = 25;
        final int interestingPoints = 50;
        final int veryInterestingPoints = 100;
        final int unclassifiedPoints = 150;

        String points;
        switch (markerInterestLevel) {
            case "#boring":
                points = Integer.toString(boringPoints);
                currentPoints += boringPoints;
                break;
            case "#notboring":
                points = Integer.toString(notBoringPoints);
                currentPoints += notBoringPoints;
                break;
            case "#interesting":
                points = Integer.toString(interestingPoints);
                currentPoints += interestingPoints;
                break;
            case "#veryinteresting":
                points = Integer.toString(veryInterestingPoints);
                currentPoints += veryInterestingPoints;
                break;
            case "#unclassified":
                points = Integer.toString(unclassifiedPoints);
                currentPoints += unclassifiedPoints;
                break;
            default:
                throw new Error("StyleUrl feed to Earned Points does not exist");
        }
        return points;
    }

    /**
     * Method updates the Total Points for the Song by adding the Current Points collected
     * and uses the Amount of Guesses to add the Guess bonus
     *
     * @param amountOfGuesses Used to compare with set values associated with the amounts
     *                        of points that should be earned for a certain amount of guesses
     */
    public void addToGuessNewTPoints(int amountOfGuesses) {
        //Uses a switch statement to update the points based
        //on guesses. Also prepares to display the total Song
        //points on the Congratulations screen

        totalSongPoints += currentPoints;

        switch (amountOfGuesses) {
            case 1:
                totalSongPoints += 500;
                break;
            case 2 | 3:
                totalSongPoints += 250;
                break;
            case 4 | 5:
                totalSongPoints += 100;
                break;
            default:
                totalSongPoints += 50;
        }
    }

    /**
     * Method Updates the total song points with a multiplier based on the difficulty level after
     * a song is completed.
     * <p>
     * It also takes into account if the difficulty was decreased or increased during play. If
     * increased there is no added points bonus but if decreased the points bonus is reduced.
     *
     * @param currentDifficulty  the current difficulty that has been set
     * @param changeInDifficulty if the difficulty has been changed, increased or decreased
     * @param initialDifficulty  what the initial difficulty was set to
     */
    public void difficultyBonusNewTPoints(int currentDifficulty, int changeInDifficulty, int initialDifficulty) {

        final int DIFFICULTY_NOT_CHANGED = 0;
        final int DIFFICULTY_DECREASED = 1;
        final int DIFFICULTY_INCREASED = 2;

        switch (changeInDifficulty) {
            //Sets currentDifficulty to initialDifficulty to avoid points bonus increase
            case DIFFICULTY_NOT_CHANGED:
                break;
            case DIFFICULTY_DECREASED:
                break;
            case DIFFICULTY_INCREASED:
                currentDifficulty = initialDifficulty;
                break;
            default:
                throw new Error("Change in difficulty has not been set correctly");
        }

        final double BONUS_VERY_EASY = 1;
        final double BONUS_EASY = 1.25;
        final double BONUS_NORMAL = 1.50;
        final double BONUS_HARD = 2.0;
        final double BONUS_VERY_HARD = 2.5;

        switch (currentDifficulty) {
            case Difficulty_VERY_EASY:
                totalSongPoints *= BONUS_VERY_EASY;
                break;
            case Difficulty_EASY:
                totalSongPoints *= BONUS_EASY;
                break;
            case Difficulty_NORMAL:
                totalSongPoints *= BONUS_NORMAL;
                break;
            case Difficulty_HARD:
                totalSongPoints *= BONUS_HARD;
                break;
            case Difficulty_VERY_HARD:
                totalSongPoints *= BONUS_VERY_HARD;
                break;
            default:
                throw new Error("Current difficulty not in range");
        }
    }

    /**
     * Method calculates the points to submit to the leaderboard depending on the state of the game
     *
     * @return points to be submitted
     */
    public int pointsToSubmit() {

        int pointsToSubmit;

        if (totalSongPoints != 0) {                      //Case after calculation by guess overlay
            pointsToSubmit = totalPoints + totalSongPoints;
        } else {                                         //Case game is quit by player during play
            pointsToSubmit = totalPoints + currentPoints;
        }

        return pointsToSubmit;
    }

    /**
     * Method updates the overall total points for the session.
     * <p>
     * It resets the current and total Song points for the next song.
     */
    public void updateTotalPoints() {

        totalPoints += totalSongPoints;
        currentPoints = 0;
        totalSongPoints = 0;
    }

    @VisibleForTesting
    void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    @VisibleForTesting
    void setTotalSongPoints(int totalSongPoints) {
        this.totalSongPoints = totalSongPoints;
    }

    @VisibleForTesting
    void setCurrentPoints(int currentPoints) {
        this.currentPoints = currentPoints;
    }

}


