package com.example.fdsfsdf.songle.utilities.XML;

import android.os.AsyncTask;
import android.util.Log;

import com.example.fdsfsdf.songle.objects.Song;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class DownloadXmlTask extends AsyncTask<URL, Void, List<Song>> { //changed class name for some reason

    @Override
    protected List<Song> doInBackground(URL... urls) {
        try {
            return loadXmlFromNetwork(urls[0]);
        } catch (IOException e) {
            throw new Error("Unable to load content. Check your network connection");
        } catch (XmlPullParserException e) {
            throw new Error("Error parsing XML");
        }
    }

    @Override
    protected void onPostExecute(List<Song> result) {
        // Do something with result
        Log.e("DownloadXML", "SongList " + "COMPLETED");
        Log.e("DownloadXML", "SongList 1stItem: " + result.get(0).getTitle());
    }

    private List<Song> loadXmlFromNetwork(URL url) throws XmlPullParserException, IOException {
        List<Song> songList;
        try (InputStream stream = downloadUrl(url)) {
            // Do something with stream e.g. parse as XML, build result

            ParsingXmlTask songDatabaseParser= new ParsingXmlTask();
            songList = songDatabaseParser.parse(stream);
            Log.e("LoadXmlFromNetwork", "DownloadedSongList empty: " + (songList.isEmpty()));
            Log.e("LoadXmlFromNetwork", "DownloadedSongList size: " + (songList.size()));

        }
        return songList;
    }

    // Given a string representation of a URL, sets up a connection and gets
    // an input stream.
    private InputStream downloadUrl(URL url) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        // Also available: HttpsURLConnection
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // Starts the query
        conn.connect();
        return conn.getInputStream();
    }


}

