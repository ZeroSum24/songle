package com.example.fdsfsdf.songle.objects;


import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class Song {
    private final String number;
    private final String artist;
    private final String title;
    private final String link;
    private Words words;

    private Map[] maps = new Map[5];
    private int mapSelectNum;

    private List<String> previousGuesses = new ArrayList<String>();



    public Song(String number, String artist, String title, String link) {
        this.number = number;
        this.artist = artist;
        this.title = title;
        this.link = link;
        this.words = null;
    }

    public String getTitle() {
        return title;
    }

    public String getNumber() { return number;}

    public void setWords(Words words) {this.words = words;}

    public Words getWords() {return words;}

    public String getArtist() {return artist;}

    public String getLink() {return link;}

    public void setMap(Map map) {
        this.maps[mapSelectNum - 1] = map;
    }

    /**
     * Method accesses the map to be loaded by the Maps activity
     *
     * @return the map to be used
     */
    public Map getMap() {
        return maps[mapSelectNum - 1];
    }

    public void setMapSelectNum(int mapSelectNum) {
        Log.e("SetMapSelectNum", "MapSelectNum contents: " + mapSelectNum);
        this.mapSelectNum = mapSelectNum;
    }

    public void setPreviousGuess(String previousGuess) {
        this.previousGuesses.add(previousGuess);
    }

    public List<String> getPreviousGuesses() {
        return previousGuesses;
    }

    /**
     * Method resets the song for use again in the case that the game has been finished and the user
     * wishes to continue playing.
     *
     * Resets the changed variables from play to null. However it does the reset in the objects
     * themselves to save the user having to re-downloaded any files.
     */
    public void resetSong() {

        for (int i = 0; i < 5; i ++) {
            if (maps[i] != null) {
                maps[i].resetPlaceMarkers();
            }
        }
        previousGuesses = new ArrayList<String>();
        words.resetAccessedWords();
    }

}
