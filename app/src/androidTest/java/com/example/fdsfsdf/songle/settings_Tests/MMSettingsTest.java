package com.example.fdsfsdf.songle.settings_Tests;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.example.fdsfsdf.songle.R;
import com.example.fdsfsdf.songle.activities.MainMenuActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MMSettingsTest {

    @Rule
    public ActivityTestRule<MainMenuActivity> mActivityTestRule = new ActivityTestRule<>(MainMenuActivity.class);

    @Test
    public void settingsTest() {
        ViewInteraction appCompatImageButton = onView(
                allOf(withId(R.id.settingsButton), withContentDescription("Click on this icon to reach the Settings menu for the Songle app."),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.wCoordinatorLayout),
                                        0),
                                6),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.sChangeDifficultyButton), withText("Change the Difficulty"),
                        childAtPosition(
                                allOf(withId(R.id.settingsMenu),
                                        childAtPosition(
                                                withClassName(is("android.support.constraint.ConstraintLayout")),
                                                5)),
                                0),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction appCompatImageButton2 = onView(
                allOf(withId(R.id.settingsButton), withContentDescription("Click on this icon to reach the Settings menu for the Songle app."),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.wCoordinatorLayout),
                                        0),
                                6),
                        isDisplayed()));
        appCompatImageButton2.perform(click());

        ViewInteraction appCompatButton2 = onView(
                allOf(withId(R.id.sHowToPlayButton), withText("How To Play"),
                        childAtPosition(
                                allOf(withId(R.id.settingsMenu),
                                        childAtPosition(
                                                withClassName(is("android.support.constraint.ConstraintLayout")),
                                                5)),
                                1),
                        isDisplayed()));
        appCompatButton2.perform(click());

        pressBack();

        ViewInteraction appCompatImageButton3 = onView(
                allOf(withId(R.id.settingsButton), withContentDescription("Click on this icon to reach the Settings menu for the Songle app."),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.wCoordinatorLayout),
                                        0),
                                6),
                        isDisplayed()));
        appCompatImageButton3.perform(click());

        ViewInteraction appCompatButton3 = onView(
                allOf(withId(R.id.sViewCompletedSongsButton), withText("View Completed Songs"),
                        childAtPosition(
                                allOf(withId(R.id.settingsMenu),
                                        childAtPosition(
                                                withClassName(is("android.support.constraint.ConstraintLayout")),
                                                5)),
                                2),
                        isDisplayed()));
        appCompatButton3.perform(click());

        pressBack();

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
