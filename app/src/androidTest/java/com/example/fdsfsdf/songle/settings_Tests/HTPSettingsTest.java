package com.example.fdsfsdf.songle.settings_Tests;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.example.fdsfsdf.songle.R;
import com.example.fdsfsdf.songle.activities.MainMenuActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class HTPSettingsTest {

    @Rule
    public ActivityTestRule<MainMenuActivity> mActivityTestRule = new ActivityTestRule<>(MainMenuActivity.class);

    @Test
    public void hTPSettingsTest() {
        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.wPlayButton), withText("Play"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.wCoordinatorLayout),
                                        0),
                                2),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction appCompatImageButton = onView(
                allOf(withId(R.id.settingsButton), withContentDescription("Click on this icon to reach the Settings menu for the Songle app."),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.wCoordinatorLayout),
                                        0),
                                6),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction appCompatImageButton2 = onView(
                allOf(withId(R.id.settingsButton), withContentDescription("Click on this icon to reach the Settings menu for the Songle app."),
                        childAtPosition(
                                allOf(withId(R.id.difficultyLayout),
                                        childAtPosition(
                                                withId(R.id.dCoordinatorLayout),
                                                0)),
                                8),
                        isDisplayed()));
        appCompatImageButton2.perform(click());

        ViewInteraction appCompatButton2 = onView(
                allOf(withId(R.id.sHowToPlayButton), withText("How To Play"),
                        childAtPosition(
                                allOf(withId(R.id.settingsMenu),
                                        childAtPosition(
                                                withId(R.id.difficultyLayout),
                                                7)),
                                1),
                        isDisplayed()));
        appCompatButton2.perform(click());

        ViewInteraction appCompatImageButton3 = onView(
                allOf(withId(R.id.settingsButton), withContentDescription("Click on this icon to reach the Settings menu for the Songle app."),
                        childAtPosition(
                                allOf(withId(R.id.howToPlayLayout),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                4),
                        isDisplayed()));
        appCompatImageButton3.perform(click());

        ViewInteraction appCompatButton3 = onView(
                allOf(withId(R.id.sChangeDifficultyButton), withText("Change the Difficulty"),
                        childAtPosition(
                                allOf(withId(R.id.settingsMenu),
                                        childAtPosition(
                                                withId(R.id.howToPlayLayout),
                                                3)),
                                0),
                        isDisplayed()));
        appCompatButton3.perform(click());

        pressBack();

        ViewInteraction appCompatButton4 = onView(
                allOf(withId(android.R.id.button2), withText("Cancel"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.ScrollView")),
                                        0),
                                2)));
        appCompatButton4.perform(scrollTo(), click());

        ViewInteraction appCompatImageButton4 = onView(
                allOf(withId(R.id.settingsButton), withContentDescription("Click on this icon to reach the Settings menu for the Songle app."),
                        childAtPosition(
                                allOf(withId(R.id.difficultyLayout),
                                        childAtPosition(
                                                withId(R.id.dCoordinatorLayout),
                                                0)),
                                8),
                        isDisplayed()));
        appCompatImageButton4.perform(click());

        ViewInteraction appCompatButton5 = onView(
                allOf(withId(R.id.sHowToPlayButton), withText("How To Play"),
                        childAtPosition(
                                allOf(withId(R.id.settingsMenu),
                                        childAtPosition(
                                                withId(R.id.difficultyLayout),
                                                7)),
                                1),
                        isDisplayed()));
        appCompatButton5.perform(click());

        ViewInteraction appCompatImageButton5 = onView(
                allOf(withId(R.id.settingsButton), withContentDescription("Click on this icon to reach the Settings menu for the Songle app."),
                        childAtPosition(
                                allOf(withId(R.id.howToPlayLayout),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                4),
                        isDisplayed()));
        appCompatImageButton5.perform(click());

        ViewInteraction appCompatButton6 = onView(
                allOf(withId(R.id.sHowToPlayButton), withText("How To Play"),
                        childAtPosition(
                                allOf(withId(R.id.settingsMenu),
                                        childAtPosition(
                                                withId(R.id.howToPlayLayout),
                                                3)),
                                1),
                        isDisplayed()));
        appCompatButton6.perform(click());

        ViewInteraction appCompatImageButton6 = onView(
                allOf(withId(R.id.settingsButton), withContentDescription("Click on this icon to reach the Settings menu for the Songle app."),
                        childAtPosition(
                                allOf(withId(R.id.howToPlayLayout),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                4),
                        isDisplayed()));
        appCompatImageButton6.perform(click());

        ViewInteraction appCompatButton7 = onView(
                allOf(withId(R.id.sViewCompletedSongsButton), withText("View Completed Songs"),
                        childAtPosition(
                                allOf(withId(R.id.settingsMenu),
                                        childAtPosition(
                                                withId(R.id.howToPlayLayout),
                                                3)),
                                2),
                        isDisplayed()));
        appCompatButton7.perform(click());

        pressBack();

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
