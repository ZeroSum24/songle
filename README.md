# Songle - Android AR Game 

> Software Engineering Large Practical - University Of Edinburgh 2018


## About 
This augmented reality app implements a location-based mobile phone puzzle game which allows users to follow a map and collect words which have been scattered at random around the University of Edinburgh�s Central Area. The words make up the lyrics of a well-known song and the puzzle aspect of the game is to guess the song from the words which have been found. Given that it is a song-based puzzle, the game is called Songle.

The design of this app was a fully-documented process with each stored in the repo. 

## Core Design Features

 - Google Play Leaderboard Integration
 - Adjustable Difficulty levels adjusting the frequency of word markers
 - Points System based on difficulty and guess success
 - Youtube song integration so songs collected can be listened to

## Install and Run

Download the repo and build the project using Android Studio. 

## Future Development

App is pending full app store release whilst further additional features are considered and an adaptable location area is implemented. 


*Songle - coming to a GooglePlay store near you.* 